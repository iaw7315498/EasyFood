using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectofinal1
{
    class AgregarFotos : PictureBox
    {
        private bool estaFavoritos;
        private const int SEPARACION = 13;
        private const int ALTURAIMAGEN = 134;
        private const int LONGITUDIMAGEN = 224;
        private int coord_x = SEPARACION;
        private int coord_y = 0;
        private PictureBox pic;
        private form1 f;

        private TabControl tabControl1;
        private Panel panel1;
        private TabPage tabPage1, tabPage2;

        public AgregarFotos(form1 f, TabControl tabControl1, Panel panel1, TabPage tabPage1, TabPage tabPage2)
        {
            this.panel1 = panel1;
            this.tabPage1 = tabPage1;
            this.tabPage2 = tabPage2;
            this.tabControl1 = tabControl1;
            this.f = f;
        }
        public void mostrarRecetas(Dictionary<Recetas, Imagenes> objeto)
        {
            mostrarRecetas(objeto, false);
        }

        public void mostrarRecetas(Dictionary<Recetas, Imagenes> objeto, bool estaFavoritos)
        {
            // borramos el panel
            panel1.Controls.Clear();
            coord_x = SEPARACION;
            coord_y = 0;

            foreach (var fila in objeto)
            {
                string direccionImagen = "imagenes\\th_" + fila.Value.Nombre + ".jpg";

                if ((coord_x + LONGITUDIMAGEN + SEPARACION) > panel1.Width)
                {
                    coord_y += ALTURAIMAGEN + SEPARACION;
                    coord_x = SEPARACION;
                }

                pic = new PictureBox();
                pic.Height = ALTURAIMAGEN;
                pic.Width = LONGITUDIMAGEN;
                pic.Parent = panel1;
                pic.Left = coord_x;
                pic.Top = coord_y;
                pic.BackColor = Color.Transparent;
                pic.Load(direccionImagen);
                pic.SizeMode = PictureBoxSizeMode.StretchImage;
                new ToolTip().SetToolTip(pic, fila.Key.Nombre);
                coord_x += pic.Width + SEPARACION;

                LinkLabel linkLabel = new LinkLabel();
                linkLabel.Parent = pic;
                linkLabel.Top = 1;
                linkLabel.Left = 1;
                linkLabel.Text = fila.Key.Nombre;
                linkLabel.Name = "linkLabel" + fila.Key.Receta_id;
                linkLabel.AutoSize = true;
                linkLabel.LinkColor = Color.Black;

                string dirFav = "";
                if (fila.Key.Favorito == true)
                {
                    dirFav = "imagenes\\fav_true.png";
                } else
                {
                    dirFav = "imagenes\\fav_false.png";
                }

                PictureBox picFav = new PictureBox();
                picFav.Parent = pic;
                picFav.Left = pic.Width - 27;
                //picFav.Top = pic.Bottom - 25;
                picFav.Height = pic.Height + 20;//
                picFav.SizeMode = PictureBoxSizeMode.StretchImage;
                Size tam = new Size();
                tam.Height = 25;
                tam.Width = 25;
                picFav.Size = tam;
                picFav.Load(dirFav);

                linkLabel.Tag = fila ;
                pic.Tag = fila;
                picFav.Tag = fila.Key;

                linkLabel.Click += new EventHandler(f.abreDetalle);
                pic.Click += new EventHandler(f.abreDetalle);

                picFav.Name = dirFav;
                picFav.Click += PicFav_Click;
                
            }
            this.estaFavoritos = estaFavoritos;
            tabControl1.SelectedTab = tabPage2;
        }

        private void PicFav_Click(object sender, EventArgs e)
        {
            PictureBox pic = (PictureBox)sender;
            string s = pic.Name;

            if (s.IndexOf("true") != -1)
            {
                s = "imagenes\\fav_false.png";
            }
            else
            {
                s = "imagenes\\fav_true.png";
            }

            pic.Name = s;
            pic.Load(s);
            f.cambiar(sender, e, estaFavoritos);
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                              