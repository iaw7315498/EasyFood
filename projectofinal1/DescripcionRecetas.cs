﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectofinal1
{
    public partial class DescripcionRecetas : Form
    {
        private const int SEPARACION = 13;
        private const int ALTURAIMAGEN = 100;
        private const int LONGITUDIMAGEN = 200;
        PictureBox pic = new PictureBox();
        //int coord_x = SEPARACION;
        //int coord_y = 0;
        public DescripcionRecetas()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Favoritos frm = new Favoritos();
            frm.Show();
            this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            form1 inicio = new form1();
            inicio.Show();
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            RecetasObtenidas frm = new RecetasObtenidas();
            frm.Show();
            this.Hide();
        }

        private void DescripcionRecetas_Load(object sender, EventArgs e)
        {
            //probar si funcionalo transparente
            label3.Parent = pictureBox1;
            label2.Parent = pictureBox1;
            label1.Parent = pictureBox1;

            // ver las imagenes dinamicamente

            //PictureBox pic = new PictureBox();

            //if ((coord_x + LONGITUDIMAGEN + SEPARACION) > this.Width)
            //{
            //    coord_y += ALTURAIMAGEN + SEPARACION;
            //    coord_x = SEPARACION;
            //}
            //else
            //{
            //    pic.Height = ALTURAIMAGEN;
            //    pic.Width = LONGITUDIMAGEN;
            //    pic.Parent = this;
            //    pic.Left = coord_x;
            //    pic.Top = coord_y;
            //    pic.BackColor = Color.Red;
            //    pic.Load("MANTEQUILLA.jpg");
            //    pic.SizeMode = PictureBoxSizeMode.StretchImage;
            //    coord_x += pic.Width + SEPARACION;
            //}
        }
    }
    
}
