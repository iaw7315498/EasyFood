﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectofinal1
{
    class Consultas
    {
        private FoodEntities food = new FoodEntities();
        private List<Recetas> listaRecetasFiltradas;
        private List<Recetas> listaRecetas;
        private List<Recetas> listaRecetasFavoritas;

        /// <summary>
        ///     Buscar recetas filtrando por ingredientes y utensilios
        /// </summary>
        /// <param name="listaIngredientes">lista de ingredientes que cogerá de una ListBox</param>
        /// <param name="listaUtensilios">lista de utensilios que cogerá de una ListBox</param>
        /// <returns>Objeto que contiene todos los campos resultantes de la busqueda filtrada por ingredientes Y utensilios</returns>
        public List<Recetas> recetasFiltradas(ListBox listaIngredientes, ListBox listaUtensilios, TextBox tiempo)
        {
            listaRecetasFiltradas = new List<Recetas>();
            string utensilios = "", ingredientes = "", consultaUtensilio = "";


            foreach (var item in listaIngredientes.Items)
            {
                ingredientes += "'" + item.ToString() + "',";
            }

            if (listaUtensilios.Items.Count > 0)
            {
                foreach (var item in listaUtensilios.Items)
                {
                    utensilios += "'" + item.ToString() + "',";
                }

                utensilios = utensilios.Substring(0, utensilios.LastIndexOf(","));

                consultaUtensilio = " and Receta_id in(select Receta_id from RecetasUtensilios inner join Utensilios " +
                    "on(RecetasUtensilios.Utensilios_id = Utensilios.Utensilios_id) where Nombre in(" + utensilios + ") " +
                    "group by Receta_id having count(Utensilios.Utensilios_id)=" + listaUtensilios.Items.Count + ") ";
            }

            ingredientes = ingredientes.Substring(0, ingredientes.LastIndexOf(","));

            string sql = "select * from Recetas where Receta_id in(select Receta_id from RecetasIngredientes " +
                "inner join Ingredientes on(RecetasIngredientes.Ingredientes_id = Ingredientes.Ingredientes_id) " +
                "where Nombre in(" + ingredientes + ") group by Receta_id having count(Ingredientes.Ingredientes_id)=" + listaIngredientes.Items.Count + ") " +
                "and Tiempo<=100";// + tiempo.Text + consultaUtensilio;

            //string sql = "select * from Recetas";
            listaRecetasFiltradas.AddRange(food.Recetas.SqlQuery(sql));

            return listaRecetasFiltradas;
        }
        /// <summary>
        ///     retornar todas las recetas que estan en la base de datos
        /// </summary>
        /// <returns>Una lista de todas las recetas</returns>
        public List<Recetas> recetas()
        {
            listaRecetas = new List<Recetas>();
            var sqlQuery = from todo in food.Recetas select todo;

            listaRecetas.AddRange(sqlQuery);

            return listaRecetas;
        }
        /// <summary>
        ///    retornar todas las recetas favoritas que tenemos en la base de datos
        /// </summary>
        /// <returns>Una lista con las recetas favoritas</returns>
        public List<Recetas> recetasFavoritas()
        {
            listaRecetasFavoritas = new List<Recetas>();
            var recFav = from b in food.Recetas where ((bool)b.Favorito == true) select b;

            listaRecetasFavoritas.AddRange(recFav);

            return listaRecetasFavoritas;
        }
        /// <summary>
        ///     Pone a favorito una receta y si ya está en favorito la quita de los favoritos
        /// </summary>
        /// <param name="receta">Objeto que contentra una receta</param>
        public void cambiarFavorito(Recetas receta)
        {
            Recetas g3 = food.Recetas.SingleOrDefault(p => p.Receta_id == receta.Receta_id);
            if (g3 != null)
            {
                g3.Favorito = !g3.Favorito; // le digo que cambie por lo contrario
                food.SaveChanges();
            }
        }
        /// <summary>
        ///     Muetra una receta aleatoria que hay en la base de datos teniendo en cuenta que se haya borrado alguna receta
        /// </summary>
        /// <returns>Objeto de una receta aleatoria</returns>
        public Recetas recetaAleatoria()
        {
            var query = from rec in food.Recetas select rec;
            int total = food.Recetas.Count(); // cuento el resultado de la query
            Recetas[] receta = new Recetas[total]; // creo un array 

            Random rad = new Random(); // creo un objeto random que me permitira coger un numero aleatorio
            double numAleatorioAux = rad.NextDouble() * total; // le indico que va a ir desde el cero al 38
            int numAleatorio = (int)numAleatorioAux; // hago un cast de double a entero (cast = cambiar el tipo)
            foreach (var item in query) // recorro las lineas de la consulta que he echo y de cada linea paso al array solo el nombre
            {
                receta[total - 1] = item;
                total--;
            }

            return receta[numAleatorio];
        }
        /// <summary>
        ///     retorna un Dictionary que tendrá como clave el objeto recetas y el valor será el objeto imagenes
        /// </summary>
        /// <param name="recetas">List</param>
        /// <returns>Dictionary</returns>
        public Dictionary<Recetas, Imagenes> recetasImagenes(List<Recetas> recetas)
        {
            Dictionary<Recetas, Imagenes> recetasImagenes = new Dictionary<Recetas, Imagenes>();

            foreach (var fila in recetas)
            {
                recetasImagenes.Add(fila, food.Imagenes.SingleOrDefault(rec => rec.Imagen_id == fila.Receta_id));
            }

            return recetasImagenes;
        }
        public void InsertarRecetaBD(string nombreReceta, ListBox ingredientes, ListBox utensilios, int tiempo, int idTipoReceta, string descripcion, string url, string nombreImagen)
        {

            Recetas nuevaReceta = new Recetas();
            string aux = nombreImagen.Replace(" ", "_");

            nuevaReceta.Nombre = nombreReceta;
            nuevaReceta.Tiempo = tiempo;
            nuevaReceta.Tipo_id = idTipoReceta;
            nuevaReceta.Elaboracion = descripcion;

            food.Recetas.Add(nuevaReceta);
            food.SaveChanges();


            int idReceta = (food.Recetas.SingleOrDefault(rece => rece.Nombre == nuevaReceta.Nombre)).Receta_id;

            string nombreImagenInsertar = "imagenes\\" + aux;
            int cont = 0;
            while (File.Exists(nombreImagenInsertar))
            {
                nombreImagenInsertar += cont;
                cont++;
            }

            File.Copy(url + "\\" + nombreImagen, nombreImagenInsertar);

            // MessageBox.Show(nombreImagenInsertar);
            //nombreImagenInsertar = nombreImagenInsertar.Substring(9, nombreImagenInsertar.LastIndexOf("."));
            //MessageBox.Show(nombreImagenInsertar);

            Imagenes nuevaImagen = new Imagenes();
            // nuevaIMagen.imagenid es el id de la receta error de la bd
            nuevaImagen.Imagen_id = idReceta;
            nuevaImagen.Nombre = aux;
            food.Imagenes.Add(nuevaImagen);
            food.SaveChanges();




            foreach (var item in ingredientes.Items)
            {
                nuevaReceta.Ingredientes.Add(food.Ingredientes.SingleOrDefault(ingr => ingr.Nombre == item.ToString()));
            }

            foreach (var item in utensilios.Items)
            {
                nuevaReceta.Utensilios.Add(food.Utensilios.SingleOrDefault(ingr => ingr.Nombre == item.ToString()));
            }
            food.SaveChanges();


        }
    }
}
