﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using System.Globalization;

namespace projectofinal1
{
    public partial class form1 : Form
    {
        
        FoodEntities food;
        private string[] arrayIngredientes;
        private string[] arrayUtensilios;

        private string nombre_imagen = "";
        private string url_imagen = "";
        private bool estaFavoritos = false;
        public form1()
        {
            InitializeComponent();
            tabControl1.Appearance = TabAppearance.FlatButtons;
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;
        }
        /// <summary>
        ///     se cargará este método al iniciar el programa y rellenará la comboBox con los arrays
        ///     que les pasaremos despues de obtenerlos de la consulta a la tabla de Ingredientes/Utensilios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Parent = pictureBox1;
            label2.Parent = pictureBox1;
            label3.Parent = pictureBox1;
            AñadirReceta.Parent = pictureBox1;
            food = new FoodEntities();

            // hacemos las consultas para saber todos los nombres de los ingredientes/utensilios
            var objetoIngrediente = from k in food.Ingredientes select k;
            var objetoUtensilios = from k in food.Utensilios select k;

            // creamos un array de cadena para dentro poner los nombres de los ingredientes/utensilios
            arrayIngredientes = new string[food.Ingredientes.Count()];
            arrayUtensilios = new string[food.Utensilios.Count()];


            // rellenamos los arrays con los nombres de los ingredientes
            int contadorArrayIngredientes = 0;
            foreach (var fila in objetoIngrediente)
            {
                arrayIngredientes[contadorArrayIngredientes] = fila.Nombre;
                contadorArrayIngredientes++;
            }

            int contadorArrayUtensilios = 0;
            foreach (var fila in objetoUtensilios)
            {
                arrayUtensilios[contadorArrayUtensilios] = fila.Nombre;
                contadorArrayUtensilios++;
            }

            //creamos dos objeto de "Autocompletar"
            var autocompletarIngredientes = new AutoCompleteStringCollection();
            var autocompletarUtensilios = new AutoCompleteStringCollection();

            // autocompletamos los ingredientes
            autocompletarIngredientes.AddRange(arrayIngredientes);
            ingredientes.AutoCompleteCustomSource = autocompletarIngredientes;
            ingredientes.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ingredientes.AutoCompleteSource = AutoCompleteSource.CustomSource;

            // autocompletamos los utensilios
            autocompletarUtensilios.AddRange(arrayUtensilios);
            utensilios.AutoCompleteCustomSource = autocompletarUtensilios;
            utensilios.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            utensilios.AutoCompleteSource = AutoCompleteSource.CustomSource;


        }
        /** FIN DE CARGA */
        
        public void cmpAgregarUtensilios()
        {
            try
            {
                // aqui le pasamos un texto a ingrediente y al ser pulsado el boton lo añadira 
                // a la lista de ingredientes
                Regex rg = new Regex(@"^\w+\s?\w+?$", RegexOptions.IgnoreCase);
                MatchCollection result = rg.Matches(utensilios.Text);

                string utensilio = utensilios.Text.ToLower();
                utensilio = Convert.ToString(utensilio[0]).ToUpper() + utensilio.Substring(1);

                if (listaUtensillios.Items.IndexOf(utensilio) == -1 &&
                    result.Count > 0 && arrayUtensilios.Contains(utensilio))
                {
                    listaUtensillios.Items.Add(utensilio);
                    muestraBotones();
                }
            }
            catch (System.IndexOutOfRangeException e)
            {
                MessageBox.Show("Error: " + e.Message + "\r Por favor, contacta con tu administrador");
            }
            finally
            {
                utensilios.Text = "";
                utensilios.Focus();
            }
        }
        //* FIN DE EVENTOS DE LOS UTENSILIOS */


        //* EVENTOS DE LOS INGREDIENTES */

        private void ingredientes_KeyPress(object sender, KeyPressEventArgs e)
        {
            cambiarLetra(sender, e);
        }



        public void cmpAgregarIngredientes()
        {
            try
            {
                // aqui le pasamos un texto a ingrediente y al ser pulsado el boton lo añadira 
                // a la lista de ingredientes
                Regex rg = new Regex(@"^\w+(\s?\w+?)+$", RegexOptions.IgnoreCase);
                MatchCollection result = rg.Matches(ingredientes.Text);

                string ingrediente = ingredientes.Text.ToLower();
                ingrediente = Convert.ToString(ingrediente[0]).ToUpper() + ingrediente.Substring(1);

                if (listaIngredientes.Items.IndexOf(ingrediente) == -1 &&
                    result.Count > 0 && arrayIngredientes.Contains(ingrediente))
                {
                    listaIngredientes.Items.Add(ingrediente);
                    muestraBotones();
                }
            }
            catch (System.IndexOutOfRangeException e)
            {
                MessageBox.Show("Error: " + e.Message + "\r Por favor, contacta con tu administrador");
            }
            finally
            {
                ingredientes.Text = "";
                ingredientes.Focus();
            }
        }
        /** FIN DE EVENTOS DE LOS INGREDIENTES */

        /** METODO PARA PASAR A MAYUSCULA LA PRIMERA LETRAS QUE INGRESAMOS EN LOS BUSCADORES */
        public void cambiarLetra(object sender, KeyPressEventArgs e)
        {
            Regex exp = new Regex("[a-z]");
            MatchCollection cadena = exp.Matches(Convert.ToString(e.KeyChar));
            Regex exp2 = new Regex("[^a-zA-Z]");
            MatchCollection simbolo = exp2.Matches(Convert.ToString(e.KeyChar));

            Keys letra = (Keys)e.KeyChar;
            if (cadena.Count > 0 && ((TextBox)sender).Text.Length == 0)
            {
                e.Handled = true;
                SendKeys.Send(Convert.ToString(e.KeyChar).ToUpper());
            }
            // que no se escriba nada si pulsamos una tecla que sea diferente a [enter, tab, space, back] o diferente a letras
            if (simbolo.Count > 0 && !(letra == Keys.Back || letra == Keys.Tab || letra == Keys.Space || letra == Keys.Enter))
            {
                e.Handled = true;
                SendKeys.Send("");
            }
        }

        /** FIN METODO PARA PASAR A MAYUSCULA LA PRIMERA LETRA */

        /** EVENTOS MOSTRAR/OCULTAR BOTONES */
        private void muestraBotones()
        {
            borrarIngrediente.Visible = (listaIngredientes.Items.Count > 0);
            borrarTodoIngredientes.Visible = (listaIngredientes.Items.Count > 1);
            borrarUtensillio.Visible = (listaUtensillios.Items.Count > 0);
            borrarTodoUtensillios.Visible = (listaUtensillios.Items.Count > 1);
        }
        /** FIN DE EVENTOS MOSTRAR/OCULTAR  BOTONES */

        /** EVENTOS CAMBIAR TIEMPO */


      

        private void BuscarRecetas_Click_1(object sender, EventArgs e)
        {
            if ((listaIngredientes.Items.Count < 1))
            {
                MessageBox.Show("Tienes que poner al menos un ingrediente");
            }
            else
            {
                Consultas con = new Consultas();
                Dictionary<Recetas, Imagenes> objeto = con.recetasImagenes(con.recetasFiltradas(listaIngredientes, listaUtensillios, tiempo));
                
                AgregarFotos agregar = new AgregarFotos(this, tabControl1, panel1, tabPage1, tabPage2);
                agregar.mostrarRecetas(objeto);
            }
        }

        private void agregarUtensillios_Click_1(object sender, EventArgs e)
        {
            cmpAgregarUtensilios();
        }

        private void scrollTiempo_Scroll(object sender, EventArgs e)
        {
            tiempo.Text = scrollTiempo.Value.ToString();
        }

        private void borrarUtensillio_Click_1(object sender, EventArgs e)
        {
            listaUtensillios.Items.Remove(listaUtensillios.SelectedItem);
            muestraBotones();
        }

        private void borrarTodoUtensillios_Click_1(object sender, EventArgs e)
        {
            listaUtensillios.Items.Clear();
            muestraBotones();
        }

        private void ingredientes_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmpAgregarIngredientes();
            if (e.KeyCode == Keys.Tab)
                utensilios.Focus();
        }

        private void agregarIngrediente_Click_1(object sender, EventArgs e)
        {
            cmpAgregarIngredientes();
        }

        private void utensilios_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            cambiarLetra(sender, e);
        }

        private void utensilios_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmpAgregarUtensilios();
            if (e.KeyCode == Keys.Tab)
                ingredientes.Focus();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage1;
            label1.Parent = pictureBox1;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Consultas con = new Consultas();
            Dictionary<Recetas, Imagenes> objeto = con.recetasImagenes(con.recetas());
       
            AgregarFotos agregar = new AgregarFotos(this, tabControl1, panel1, tabPage1, tabPage2);
            agregar.mostrarRecetas(objeto);
            label2.Parent = pictureBox1;
        }

        public void abreDetalle(object sender, EventArgs e)
        {
            KeyValuePair<Recetas, Imagenes> rec;

            string tipo = sender.GetType().ToString().Substring(sender.GetType().ToString().LastIndexOf(".") + 1);
            if (tipo.Equals("LinkLabel"))
            {
                rec = (KeyValuePair<Recetas, Imagenes>)((LinkLabel)sender).Tag;
            }
            else
            {
                rec = (KeyValuePair<Recetas, Imagenes>)((PictureBox)sender).Tag;
            }
            
            Imagenes imagen = rec.Value;
            Recetas receta = rec.Key;
            
            int indexPasos = receta.Elaboracion.IndexOf("PASOS");
            string herramientas = receta.Elaboracion.Substring(0, indexPasos).Trim();
            string elaboracion = receta.Elaboracion.Substring(indexPasos + 6).Trim();

            textBox1.Text = receta.Nombre;
            textBox2.Text = herramientas;
            richTextBox1.Text = elaboracion;
            pictureBox3.Load("imagenes\\th_" + imagen.Nombre + ".jpg");
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            tabControl1.SelectedTab = tabPage3;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Consultas con = new Consultas();
            Dictionary<Recetas, Imagenes> objeto = con.recetasImagenes(con.recetasFavoritas());

            AgregarFotos agregar = new AgregarFotos(this, tabControl1, panel1, tabPage1, tabPage2);
            agregar.mostrarRecetas(objeto, true);
            label3.Parent = pictureBox1;

        }


        private void pictureBox2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage3;
        }

        private void borrarIngrediente_Click(object sender, EventArgs e)
        {
            listaIngredientes.Items.Remove(listaIngredientes.SelectedItem);
            muestraBotones();
        }

        private void borrarTodoIngredientes_Click(object sender, EventArgs e)
        {
            listaIngredientes.Items.Clear();
            muestraBotones();
        }
        public void cambiar(object sender, EventArgs e, bool estaFavoritos)
        {
            Consultas con = new Consultas();
            Recetas receta = (Recetas)((PictureBox)sender).Tag;
            con.cambiarFavorito(receta);
            if (estaFavoritos == true)
                label3_Click(sender, e);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void scrollTiempoInsertar_Scroll(object sender, EventArgs e)
        {

        }

        private void borrarTodoUtensilioInsertar_Click(object sender, EventArgs e)
        {

        }

        private void borrarUtensiliosInsertar_Click(object sender, EventArgs e)
        {

        }

        private void anadirUtensiliosInsertar_Click(object sender, EventArgs e)
        {

        }

        private void borrarTodoIngredientesInsertar_Click(object sender, EventArgs e)
        {

        }

        private void borrarIngredientesInsertar_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void anadirIngredientesInsertar_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void ingredientesInsertar_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void anadirIngredientesInsertar_Click_1(object sender, EventArgs e)
        {
            Regex rg = new Regex(@"^\w+(\s?\w+?)+$", RegexOptions.IgnoreCase);
            MatchCollection result = rg.Matches(ingredientesInsertar.Text);

            string ingrediente = ingredientesInsertar.Text.ToLower();
            ingrediente = Convert.ToString(ingrediente[0]).ToUpper() + ingrediente.Substring(1);

            if (listaIngredientesInsertar.Items.IndexOf(ingrediente) == -1 &&
                result.Count > 0 && arrayIngredientes.Contains(ingrediente))
            {
                listaIngredientesInsertar.Items.Add(ingrediente);
                ingredientesInsertar.Text = "";

                borrarIngredientesInsertar.Visible = (listaIngredientesInsertar.Items.Count > 0);
                borrarTodoIngredientesInsertar.Visible = (listaIngredientesInsertar.Items.Count > 1);

            }
        }

        private void borrarIngredientesInsertar_Click_1(object sender, EventArgs e)
        {
            listaIngredientesInsertar.Items.Remove(listaIngredientesInsertar.SelectedItem);
            borrarIngredientesInsertar.Visible = (listaIngredientesInsertar.Items.Count > 0);
            borrarTodoIngredientesInsertar.Visible = (listaIngredientesInsertar.Items.Count > 1);


        }

        private void borrarTodoIngredientesInsertar_Click_1(object sender, EventArgs e)
        {

            listaIngredientesInsertar.Items.Clear();
            borrarIngredientesInsertar.Visible = (listaIngredientesInsertar.Items.Count > 0);
            borrarTodoIngredientesInsertar.Visible = (listaIngredientesInsertar.Items.Count > 1);
        }

        private void anadirUtensiliosInsertar_Click_1(object sender, EventArgs e)
        {
            Regex rg = new Regex(@"^\w+\s?\w+?$", RegexOptions.IgnoreCase);
            MatchCollection result = rg.Matches(utensiliosInsertar.Text);

            string utensilio = utensiliosInsertar.Text.ToLower();
            utensilio = Convert.ToString(utensilio[0]).ToUpper() + utensilio.Substring(1);

            if (listaUtensiliosInsertar.Items.IndexOf(utensilio) == -1 &&
                result.Count > 0 && arrayUtensilios.Contains(utensilio))
            {
                listaUtensiliosInsertar.Items.Add(utensilio);
                utensiliosInsertar.Text = "";

                borrarUtensiliosInsertar.Visible = (listaUtensiliosInsertar.Items.Count > 0);
                borrarTodoUtensilioInsertar.Visible = (listaUtensiliosInsertar.Items.Count > 1);

            }
        }

        private void borrarUtensiliosInsertar_Click_1(object sender, EventArgs e)
        {

            listaUtensiliosInsertar.Items.Remove(listaUtensiliosInsertar.SelectedItem);

            borrarUtensiliosInsertar.Visible = (listaUtensiliosInsertar.Items.Count > 0);
            borrarTodoUtensilioInsertar.Visible = (listaUtensiliosInsertar.Items.Count > 1);
        }

        private void borrarTodoUtensilioInsertar_Click_1(object sender, EventArgs e)
        {
            listaUtensiliosInsertar.Items.Clear();
            borrarUtensiliosInsertar.Visible = (listaUtensiliosInsertar.Items.Count > 0);
            borrarTodoUtensilioInsertar.Visible = (listaUtensiliosInsertar.Items.Count > 1);
        }

        private void scrollTiempoInsertar_Scroll_1(object sender, EventArgs e)
        {
            tiempoInsertar.Text = scrollTiempoInsertar.Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "JPG(*.jpg)|*.jpg|PNG(*.png)|*.png|GIF(*& *.Png, *.Gif, *.Tiff, *.Jpeg, *.Bmp)|*.Jpg; *.Png; *.Gif; *." + "Tiff; *.Jpeg; *.Bmp";
            openFileDialog1.Title = "Abrir solo imagenes";


            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                nombre_imagen = System.IO.Path.GetFileName(openFileDialog1.FileName);
                url_imagen = System.IO.Path.GetDirectoryName(openFileDialog1.FileName);

                MessageBox.Show(nombre_imagen);
                MessageBox.Show(url_imagen);

                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox2.Load(url_imagen + "\\" + nombre_imagen);

            }
        }

        private void insertarReceta_Click(object sender, EventArgs e)
        {
            int idTipoReceta = 0;

            if (nombreReceta.Text == "")
            {
                MessageBox.Show("Por favor introdusca el nombre de la receta");
            }

            else if (listaIngredientesInsertar.Items.Count < 1)
            {
                MessageBox.Show("Una receta debe de tener como mínimo un ingrediente");
            }

            else if (listaUtensiliosInsertar.Items.Count < 1)
            {
                MessageBox.Show("Debe introducir al menos un utensilio");
            }

            else if (descripcionReceta.Size == null)
            {
                MessageBox.Show("Cada receta debe tener una descripción");
            }

            else if (Convert.ToInt32(tiempoInsertar.Text) == 0)
            {
                MessageBox.Show("Una receta debe de tener asociado un tiempo mayor que 0");

            }

            if (entrante.Checked == true)
            {
                idTipoReceta = 1;
            }
            else if (principal.Checked == true)
            {
                idTipoReceta = 2;
            }
            else if (postre.Checked == true)
            {
                idTipoReceta = 3;
            }
            else
            {
                MessageBox.Show("Por favor especifique si la receta es entrante, principal o postre");
            }


            Consultas con = new Consultas();

            con.InsertarRecetaBD(nombreReceta.Text, listaIngredientesInsertar, listaUtensiliosInsertar, Convert.ToInt16(tiempoInsertar.Text), idTipoReceta, descripcionReceta.Text, url_imagen, nombre_imagen);



        }

        private void AñadirReceta_Click(object sender, EventArgs e)
        {
            var autocompletarIngredientes = new AutoCompleteStringCollection();
            var autocompletarUtensilios = new AutoCompleteStringCollection();

            // autocompletamos los ingredientes
            autocompletarIngredientes.AddRange(arrayIngredientes);
            ingredientesInsertar.AutoCompleteCustomSource = autocompletarIngredientes;
            ingredientesInsertar.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ingredientesInsertar.AutoCompleteSource = AutoCompleteSource.CustomSource;




            // autocompletamos los utensilios
            autocompletarUtensilios.AddRange(arrayUtensilios);
            utensiliosInsertar.AutoCompleteCustomSource = autocompletarUtensilios;
            utensiliosInsertar.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            utensiliosInsertar.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tabControl1.SelectedTab = tabPage4;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}