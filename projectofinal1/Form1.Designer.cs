﻿namespace projectofinal1
{
    partial class form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.insertarReceta = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.postre = new System.Windows.Forms.RadioButton();
            this.principal = new System.Windows.Forms.RadioButton();
            this.entrante = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.descripcionReceta = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tiempoInsertar = new System.Windows.Forms.TextBox();
            this.scrollTiempoInsertar = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.borrarTodoUtensilioInsertar = new System.Windows.Forms.Button();
            this.borrarUtensiliosInsertar = new System.Windows.Forms.Button();
            this.anadirUtensiliosInsertar = new System.Windows.Forms.Button();
            this.listaUtensiliosInsertar = new System.Windows.Forms.ListBox();
            this.utensiliosInsertar = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.borrarTodoIngredientesInsertar = new System.Windows.Forms.Button();
            this.borrarIngredientesInsertar = new System.Windows.Forms.Button();
            this.listaIngredientesInsertar = new System.Windows.Forms.ListBox();
            this.anadirIngredientesInsertar = new System.Windows.Forms.Button();
            this.ingredientesInsertar = new System.Windows.Forms.TextBox();
            this.nombreReceta = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.BuscarRecetas = new System.Windows.Forms.Button();
            this.tiempo = new System.Windows.Forms.TextBox();
            this.utensilios = new System.Windows.Forms.TextBox();
            this.ingredientes = new System.Windows.Forms.TextBox();
            this.scrollTiempo = new System.Windows.Forms.TrackBar();
            this.listaUtensillios = new System.Windows.Forms.ListBox();
            this.borrarTodoUtensillios = new System.Windows.Forms.Button();
            this.borrarUtensillio = new System.Windows.Forms.Button();
            this.agregarUtensillios = new System.Windows.Forms.Button();
            this.borrarTodoIngredientes = new System.Windows.Forms.Button();
            this.borrarIngrediente = new System.Windows.Forms.Button();
            this.agregarIngrediente = new System.Windows.Forms.Button();
            this.listaIngredientes = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AñadirReceta = new System.Windows.Forms.Label();
            this.tabPage4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scrollTiempoInsertar)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scrollTiempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.label1.Location = new System.Drawing.Point(67, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 19);
            this.label1.TabIndex = 30;
            this.label1.Text = "Inicio";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.label2.Location = new System.Drawing.Point(237, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 19);
            this.label2.TabIndex = 31;
            this.label2.Text = "Recetas";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.label3.Location = new System.Drawing.Point(400, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 19);
            this.label3.TabIndex = 32;
            this.label3.Text = "Favoritos";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(725, 267);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.tabPage4.Controls.Add(this.insertarReceta);
            this.tabPage4.Controls.Add(this.button2);
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(725, 267);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            // 
            // insertarReceta
            // 
            this.insertarReceta.Location = new System.Drawing.Point(582, 234);
            this.insertarReceta.Name = "insertarReceta";
            this.insertarReceta.Size = new System.Drawing.Size(110, 23);
            this.insertarReceta.TabIndex = 8;
            this.insertarReceta.Text = "Insertar Receta";
            this.insertarReceta.UseVisualStyleBackColor = true;
            this.insertarReceta.Click += new System.EventHandler(this.insertarReceta_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(595, 184);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Examinar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pictureBox4);
            this.groupBox6.Location = new System.Drawing.Point(563, 8);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(154, 162);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Imagen";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(5, 17);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(143, 137);
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.nombreReceta);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(558, 264);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Receta";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.postre);
            this.groupBox7.Controls.Add(this.principal);
            this.groupBox7.Controls.Add(this.entrante);
            this.groupBox7.Location = new System.Drawing.Point(160, 181);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(108, 84);
            this.groupBox7.TabIndex = 43;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Tipo Menu";
            // 
            // postre
            // 
            this.postre.AutoSize = true;
            this.postre.Location = new System.Drawing.Point(16, 67);
            this.postre.Name = "postre";
            this.postre.Size = new System.Drawing.Size(55, 17);
            this.postre.TabIndex = 2;
            this.postre.TabStop = true;
            this.postre.Text = "Postre";
            this.postre.UseVisualStyleBackColor = true;
            // 
            // principal
            // 
            this.principal.AutoSize = true;
            this.principal.Location = new System.Drawing.Point(16, 43);
            this.principal.Name = "principal";
            this.principal.Size = new System.Drawing.Size(65, 17);
            this.principal.TabIndex = 1;
            this.principal.TabStop = true;
            this.principal.Text = "Principal";
            this.principal.UseVisualStyleBackColor = true;
            // 
            // entrante
            // 
            this.entrante.AutoSize = true;
            this.entrante.Location = new System.Drawing.Point(16, 20);
            this.entrante.Name = "entrante";
            this.entrante.Size = new System.Drawing.Size(65, 17);
            this.entrante.TabIndex = 0;
            this.entrante.TabStop = true;
            this.entrante.Text = "Entrante";
            this.entrante.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.descripcionReceta);
            this.groupBox5.Location = new System.Drawing.Point(290, 188);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(252, 100);
            this.groupBox5.TabIndex = 42;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Descripción";
            // 
            // descripcionReceta
            // 
            this.descripcionReceta.Location = new System.Drawing.Point(6, 16);
            this.descripcionReceta.Multiline = true;
            this.descripcionReceta.Name = "descripcionReceta";
            this.descripcionReceta.Size = new System.Drawing.Size(231, 50);
            this.descripcionReceta.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tiempoInsertar);
            this.groupBox4.Controls.Add(this.scrollTiempoInsertar);
            this.groupBox4.Location = new System.Drawing.Point(11, 181);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(150, 84);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tiempo";
            // 
            // tiempoInsertar
            // 
            this.tiempoInsertar.Location = new System.Drawing.Point(55, 58);
            this.tiempoInsertar.Name = "tiempoInsertar";
            this.tiempoInsertar.Size = new System.Drawing.Size(32, 20);
            this.tiempoInsertar.TabIndex = 38;
            this.tiempoInsertar.Text = "0";
            // 
            // scrollTiempoInsertar
            // 
            this.scrollTiempoInsertar.Location = new System.Drawing.Point(25, 17);
            this.scrollTiempoInsertar.Maximum = 180;
            this.scrollTiempoInsertar.Name = "scrollTiempoInsertar";
            this.scrollTiempoInsertar.Size = new System.Drawing.Size(82, 45);
            this.scrollTiempoInsertar.SmallChange = 5;
            this.scrollTiempoInsertar.TabIndex = 37;
            this.scrollTiempoInsertar.TickFrequency = 15;
            this.scrollTiempoInsertar.Value = 30;
            this.scrollTiempoInsertar.Scroll += new System.EventHandler(this.scrollTiempoInsertar_Scroll_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.borrarTodoUtensilioInsertar);
            this.groupBox3.Controls.Add(this.borrarUtensiliosInsertar);
            this.groupBox3.Controls.Add(this.anadirUtensiliosInsertar);
            this.groupBox3.Controls.Add(this.listaUtensiliosInsertar);
            this.groupBox3.Controls.Add(this.utensiliosInsertar);
            this.groupBox3.Location = new System.Drawing.Point(290, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(252, 140);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Utensilios";
            // 
            // borrarTodoUtensilioInsertar
            // 
            this.borrarTodoUtensilioInsertar.Location = new System.Drawing.Point(169, 97);
            this.borrarTodoUtensilioInsertar.Name = "borrarTodoUtensilioInsertar";
            this.borrarTodoUtensilioInsertar.Size = new System.Drawing.Size(77, 20);
            this.borrarTodoUtensilioInsertar.TabIndex = 39;
            this.borrarTodoUtensilioInsertar.Text = "Borrar Todo";
            this.borrarTodoUtensilioInsertar.UseVisualStyleBackColor = true;
            this.borrarTodoUtensilioInsertar.Visible = false;
            this.borrarTodoUtensilioInsertar.Click += new System.EventHandler(this.borrarTodoUtensilioInsertar_Click_1);
            // 
            // borrarUtensiliosInsertar
            // 
            this.borrarUtensiliosInsertar.Location = new System.Drawing.Point(169, 62);
            this.borrarUtensiliosInsertar.Name = "borrarUtensiliosInsertar";
            this.borrarUtensiliosInsertar.Size = new System.Drawing.Size(77, 20);
            this.borrarUtensiliosInsertar.TabIndex = 38;
            this.borrarUtensiliosInsertar.Text = "Borrar";
            this.borrarUtensiliosInsertar.UseVisualStyleBackColor = true;
            this.borrarUtensiliosInsertar.Visible = false;
            this.borrarUtensiliosInsertar.Click += new System.EventHandler(this.borrarUtensiliosInsertar_Click_1);
            // 
            // anadirUtensiliosInsertar
            // 
            this.anadirUtensiliosInsertar.Location = new System.Drawing.Point(169, 21);
            this.anadirUtensiliosInsertar.Name = "anadirUtensiliosInsertar";
            this.anadirUtensiliosInsertar.Size = new System.Drawing.Size(77, 20);
            this.anadirUtensiliosInsertar.TabIndex = 37;
            this.anadirUtensiliosInsertar.Text = "Añadir";
            this.anadirUtensiliosInsertar.UseVisualStyleBackColor = true;
            this.anadirUtensiliosInsertar.Click += new System.EventHandler(this.anadirUtensiliosInsertar_Click_1);
            // 
            // listaUtensiliosInsertar
            // 
            this.listaUtensiliosInsertar.FormattingEnabled = true;
            this.listaUtensiliosInsertar.Location = new System.Drawing.Point(9, 47);
            this.listaUtensiliosInsertar.Name = "listaUtensiliosInsertar";
            this.listaUtensiliosInsertar.Size = new System.Drawing.Size(152, 82);
            this.listaUtensiliosInsertar.TabIndex = 36;
            // 
            // utensiliosInsertar
            // 
            this.utensiliosInsertar.Location = new System.Drawing.Point(9, 19);
            this.utensiliosInsertar.Name = "utensiliosInsertar";
            this.utensiliosInsertar.Size = new System.Drawing.Size(152, 20);
            this.utensiliosInsertar.TabIndex = 35;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.borrarTodoIngredientesInsertar);
            this.groupBox2.Controls.Add(this.borrarIngredientesInsertar);
            this.groupBox2.Controls.Add(this.listaIngredientesInsertar);
            this.groupBox2.Controls.Add(this.anadirIngredientesInsertar);
            this.groupBox2.Controls.Add(this.ingredientesInsertar);
            this.groupBox2.Location = new System.Drawing.Point(11, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 133);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ingredientes";
            // 
            // borrarTodoIngredientesInsertar
            // 
            this.borrarTodoIngredientesInsertar.Location = new System.Drawing.Point(174, 105);
            this.borrarTodoIngredientesInsertar.Name = "borrarTodoIngredientesInsertar";
            this.borrarTodoIngredientesInsertar.Size = new System.Drawing.Size(71, 20);
            this.borrarTodoIngredientesInsertar.TabIndex = 32;
            this.borrarTodoIngredientesInsertar.Text = "Borrar Todo";
            this.borrarTodoIngredientesInsertar.UseVisualStyleBackColor = true;
            this.borrarTodoIngredientesInsertar.Visible = false;
            this.borrarTodoIngredientesInsertar.Click += new System.EventHandler(this.borrarTodoIngredientesInsertar_Click_1);
            // 
            // borrarIngredientesInsertar
            // 
            this.borrarIngredientesInsertar.Location = new System.Drawing.Point(174, 63);
            this.borrarIngredientesInsertar.Name = "borrarIngredientesInsertar";
            this.borrarIngredientesInsertar.Size = new System.Drawing.Size(71, 20);
            this.borrarIngredientesInsertar.TabIndex = 31;
            this.borrarIngredientesInsertar.Text = "Borrar";
            this.borrarIngredientesInsertar.UseVisualStyleBackColor = true;
            this.borrarIngredientesInsertar.Visible = false;
            this.borrarIngredientesInsertar.Click += new System.EventHandler(this.borrarIngredientesInsertar_Click_1);
            // 
            // listaIngredientesInsertar
            // 
            this.listaIngredientesInsertar.FormattingEnabled = true;
            this.listaIngredientesInsertar.Location = new System.Drawing.Point(12, 47);
            this.listaIngredientesInsertar.Name = "listaIngredientesInsertar";
            this.listaIngredientesInsertar.Size = new System.Drawing.Size(142, 82);
            this.listaIngredientesInsertar.TabIndex = 30;
            // 
            // anadirIngredientesInsertar
            // 
            this.anadirIngredientesInsertar.Location = new System.Drawing.Point(174, 23);
            this.anadirIngredientesInsertar.Name = "anadirIngredientesInsertar";
            this.anadirIngredientesInsertar.Size = new System.Drawing.Size(71, 20);
            this.anadirIngredientesInsertar.TabIndex = 29;
            this.anadirIngredientesInsertar.Text = "Añadir";
            this.anadirIngredientesInsertar.UseVisualStyleBackColor = true;
            this.anadirIngredientesInsertar.Click += new System.EventHandler(this.anadirIngredientesInsertar_Click_1);
            // 
            // ingredientesInsertar
            // 
            this.ingredientesInsertar.AcceptsReturn = true;
            this.ingredientesInsertar.Location = new System.Drawing.Point(12, 20);
            this.ingredientesInsertar.MaxLength = 30;
            this.ingredientesInsertar.Name = "ingredientesInsertar";
            this.ingredientesInsertar.Size = new System.Drawing.Size(142, 20);
            this.ingredientesInsertar.TabIndex = 28;
            // 
            // nombreReceta
            // 
            this.nombreReceta.Location = new System.Drawing.Point(104, 16);
            this.nombreReceta.Name = "nombreReceta";
            this.nombreReceta.Size = new System.Drawing.Size(417, 20);
            this.nombreReceta.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nombre Receta";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.tabPage3.Controls.Add(this.pictureBox3);
            this.tabPage3.Controls.Add(this.textBox2);
            this.tabPage3.Controls.Add(this.textBox1);
            this.tabPage3.Controls.Add(this.richTextBox1);
            this.tabPage3.Controls.Add(this.pictureBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(725, 267);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(433, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(289, 134);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(102, 42);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(283, 98);
            this.textBox2.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(132, 11);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(194, 21);
            this.textBox1.TabIndex = 3;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.BulletIndent = 1;
            this.richTextBox1.Location = new System.Drawing.Point(7, 147);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(702, 134);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::projectofinal1.Properties.Resources.flecha;
            this.pictureBox2.Location = new System.Drawing.Point(7, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(725, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.panel1.Location = new System.Drawing.Point(-4, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(729, 289);
            this.panel1.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(1, 164);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(733, 293);
            this.tabControl1.TabIndex = 29;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.tabPage1.BackgroundImage = global::projectofinal1.Properties.Resources.diseñofinal_03;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage1.Controls.Add(this.BuscarRecetas);
            this.tabPage1.Controls.Add(this.tiempo);
            this.tabPage1.Controls.Add(this.utensilios);
            this.tabPage1.Controls.Add(this.ingredientes);
            this.tabPage1.Controls.Add(this.scrollTiempo);
            this.tabPage1.Controls.Add(this.listaUtensillios);
            this.tabPage1.Controls.Add(this.borrarTodoUtensillios);
            this.tabPage1.Controls.Add(this.borrarUtensillio);
            this.tabPage1.Controls.Add(this.agregarUtensillios);
            this.tabPage1.Controls.Add(this.borrarTodoIngredientes);
            this.tabPage1.Controls.Add(this.borrarIngrediente);
            this.tabPage1.Controls.Add(this.agregarIngrediente);
            this.tabPage1.Controls.Add(this.listaIngredientes);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(725, 267);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // BuscarRecetas
            // 
            this.BuscarRecetas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.BuscarRecetas.FlatAppearance.BorderSize = 0;
            this.BuscarRecetas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BuscarRecetas.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuscarRecetas.Location = new System.Drawing.Point(326, 218);
            this.BuscarRecetas.Name = "BuscarRecetas";
            this.BuscarRecetas.Size = new System.Drawing.Size(70, 40);
            this.BuscarRecetas.TabIndex = 37;
            this.BuscarRecetas.Text = "Obtener Recetas";
            this.BuscarRecetas.UseVisualStyleBackColor = false;
            this.BuscarRecetas.Click += new System.EventHandler(this.BuscarRecetas_Click_1);
            // 
            // tiempo
            // 
            this.tiempo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tiempo.Location = new System.Drawing.Point(343, 141);
            this.tiempo.Name = "tiempo";
            this.tiempo.Size = new System.Drawing.Size(32, 13);
            this.tiempo.TabIndex = 36;
            this.tiempo.Text = "30";
            this.tiempo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // utensilios
            // 
            this.utensilios.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.utensilios.Location = new System.Drawing.Point(535, 133);
            this.utensilios.Name = "utensilios";
            this.utensilios.Size = new System.Drawing.Size(152, 21);
            this.utensilios.TabIndex = 33;
            this.utensilios.KeyDown += new System.Windows.Forms.KeyEventHandler(this.utensilios_KeyDown_1);
            this.utensilios.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.utensilios_KeyPress_1);
            // 
            // ingredientes
            // 
            this.ingredientes.AcceptsReturn = true;
            this.ingredientes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientes.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ingredientes.Location = new System.Drawing.Point(31, 133);
            this.ingredientes.MaxLength = 30;
            this.ingredientes.Name = "ingredientes";
            this.ingredientes.Size = new System.Drawing.Size(142, 21);
            this.ingredientes.TabIndex = 26;
            this.ingredientes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ingredientes_KeyDown_1);
            this.ingredientes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ingredientes_KeyPress);
            // 
            // scrollTiempo
            // 
            this.scrollTiempo.AutoSize = false;
            this.scrollTiempo.Location = new System.Drawing.Point(326, 88);
            this.scrollTiempo.Maximum = 180;
            this.scrollTiempo.Name = "scrollTiempo";
            this.scrollTiempo.Size = new System.Drawing.Size(70, 40);
            this.scrollTiempo.SmallChange = 5;
            this.scrollTiempo.TabIndex = 35;
            this.scrollTiempo.TickFrequency = 15;
            this.scrollTiempo.Value = 30;
            this.scrollTiempo.Scroll += new System.EventHandler(this.scrollTiempo_Scroll);
            // 
            // listaUtensillios
            // 
            this.listaUtensillios.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listaUtensillios.FormattingEnabled = true;
            this.listaUtensillios.Location = new System.Drawing.Point(535, 173);
            this.listaUtensillios.Name = "listaUtensillios";
            this.listaUtensillios.Size = new System.Drawing.Size(152, 82);
            this.listaUtensillios.TabIndex = 34;
            // 
            // borrarTodoUtensillios
            // 
            this.borrarTodoUtensillios.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.borrarTodoUtensillios.FlatAppearance.BorderSize = 2;
            this.borrarTodoUtensillios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.borrarTodoUtensillios.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borrarTodoUtensillios.Location = new System.Drawing.Point(436, 218);
            this.borrarTodoUtensillios.Name = "borrarTodoUtensillios";
            this.borrarTodoUtensillios.Size = new System.Drawing.Size(70, 40);
            this.borrarTodoUtensillios.TabIndex = 32;
            this.borrarTodoUtensillios.Text = "Borrar Todo";
            this.borrarTodoUtensillios.UseVisualStyleBackColor = true;
            this.borrarTodoUtensillios.Visible = false;
            this.borrarTodoUtensillios.Click += new System.EventHandler(this.borrarTodoUtensillios_Click_1);
            // 
            // borrarUtensillio
            // 
            this.borrarUtensillio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.borrarUtensillio.FlatAppearance.BorderSize = 0;
            this.borrarUtensillio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.borrarUtensillio.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borrarUtensillio.Location = new System.Drawing.Point(436, 172);
            this.borrarUtensillio.Name = "borrarUtensillio";
            this.borrarUtensillio.Size = new System.Drawing.Size(70, 23);
            this.borrarUtensillio.TabIndex = 31;
            this.borrarUtensillio.Text = "Borrar";
            this.borrarUtensillio.UseVisualStyleBackColor = false;
            this.borrarUtensillio.Visible = false;
            this.borrarUtensillio.Click += new System.EventHandler(this.borrarUtensillio_Click_1);
            // 
            // agregarUtensillios
            // 
            this.agregarUtensillios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.agregarUtensillios.FlatAppearance.BorderSize = 0;
            this.agregarUtensillios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarUtensillios.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarUtensillios.Location = new System.Drawing.Point(436, 133);
            this.agregarUtensillios.Name = "agregarUtensillios";
            this.agregarUtensillios.Size = new System.Drawing.Size(70, 23);
            this.agregarUtensillios.TabIndex = 30;
            this.agregarUtensillios.Text = "Añadir";
            this.agregarUtensillios.UseVisualStyleBackColor = false;
            this.agregarUtensillios.Click += new System.EventHandler(this.agregarUtensillios_Click_1);
            // 
            // borrarTodoIngredientes
            // 
            this.borrarTodoIngredientes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.borrarTodoIngredientes.FlatAppearance.BorderSize = 2;
            this.borrarTodoIngredientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.borrarTodoIngredientes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borrarTodoIngredientes.Location = new System.Drawing.Point(197, 218);
            this.borrarTodoIngredientes.Name = "borrarTodoIngredientes";
            this.borrarTodoIngredientes.Size = new System.Drawing.Size(70, 40);
            this.borrarTodoIngredientes.TabIndex = 29;
            this.borrarTodoIngredientes.Text = "Borrar Todo";
            this.borrarTodoIngredientes.UseVisualStyleBackColor = true;
            this.borrarTodoIngredientes.Visible = false;
            this.borrarTodoIngredientes.Click += new System.EventHandler(this.borrarTodoIngredientes_Click);
            // 
            // borrarIngrediente
            // 
            this.borrarIngrediente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.borrarIngrediente.FlatAppearance.BorderSize = 0;
            this.borrarIngrediente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.borrarIngrediente.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.borrarIngrediente.Location = new System.Drawing.Point(197, 173);
            this.borrarIngrediente.Name = "borrarIngrediente";
            this.borrarIngrediente.Size = new System.Drawing.Size(70, 23);
            this.borrarIngrediente.TabIndex = 28;
            this.borrarIngrediente.Text = "Borrar";
            this.borrarIngrediente.UseVisualStyleBackColor = false;
            this.borrarIngrediente.Visible = false;
            this.borrarIngrediente.Click += new System.EventHandler(this.borrarIngrediente_Click);
            // 
            // agregarIngrediente
            // 
            this.agregarIngrediente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(193)))), ((int)(((byte)(55)))));
            this.agregarIngrediente.FlatAppearance.BorderSize = 0;
            this.agregarIngrediente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarIngrediente.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarIngrediente.Location = new System.Drawing.Point(197, 134);
            this.agregarIngrediente.Name = "agregarIngrediente";
            this.agregarIngrediente.Size = new System.Drawing.Size(70, 23);
            this.agregarIngrediente.TabIndex = 27;
            this.agregarIngrediente.Text = "Añadir";
            this.agregarIngrediente.UseVisualStyleBackColor = false;
            this.agregarIngrediente.Click += new System.EventHandler(this.agregarIngrediente_Click_1);
            // 
            // listaIngredientes
            // 
            this.listaIngredientes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listaIngredientes.FormattingEnabled = true;
            this.listaIngredientes.Location = new System.Drawing.Point(31, 173);
            this.listaIngredientes.Name = "listaIngredientes";
            this.listaIngredientes.Size = new System.Drawing.Size(142, 82);
            this.listaIngredientes.TabIndex = 25;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::projectofinal1.Properties.Resources.header;
            this.pictureBox1.Location = new System.Drawing.Point(1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(733, 158);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // AñadirReceta
            // 
            this.AñadirReceta.AutoEllipsis = true;
            this.AñadirReceta.BackColor = System.Drawing.Color.Transparent;
            this.AñadirReceta.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AñadirReceta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.AñadirReceta.Location = new System.Drawing.Point(554, 136);
            this.AñadirReceta.Name = "AñadirReceta";
            this.AñadirReceta.Size = new System.Drawing.Size(143, 19);
            this.AñadirReceta.TabIndex = 33;
            this.AñadirReceta.Text = "Añadir Recetas";
            this.AñadirReceta.Click += new System.EventHandler(this.AñadirReceta_Click);
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(205)))));
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.AñadirReceta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(750, 500);
            this.MinimizeBox = false;
            this.Name = "form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scrollTiempoInsertar)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scrollTiempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button BuscarRecetas;
        private System.Windows.Forms.TextBox tiempo;
        private System.Windows.Forms.TextBox utensilios;
        private System.Windows.Forms.TextBox ingredientes;
        private System.Windows.Forms.TrackBar scrollTiempo;
        private System.Windows.Forms.ListBox listaUtensillios;
        private System.Windows.Forms.Button borrarTodoUtensillios;
        private System.Windows.Forms.Button borrarUtensillio;
        private System.Windows.Forms.Button agregarUtensillios;
        private System.Windows.Forms.Button borrarTodoIngredientes;
        private System.Windows.Forms.Button borrarIngrediente;
        private System.Windows.Forms.Button agregarIngrediente;
        private System.Windows.Forms.ListBox listaIngredientes;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button insertarReceta;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton postre;
        private System.Windows.Forms.RadioButton principal;
        private System.Windows.Forms.RadioButton entrante;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox descripcionReceta;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tiempoInsertar;
        private System.Windows.Forms.TrackBar scrollTiempoInsertar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button borrarTodoUtensilioInsertar;
        private System.Windows.Forms.Button borrarUtensiliosInsertar;
        private System.Windows.Forms.Button anadirUtensiliosInsertar;
        private System.Windows.Forms.ListBox listaUtensiliosInsertar;
        private System.Windows.Forms.TextBox utensiliosInsertar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button borrarTodoIngredientesInsertar;
        private System.Windows.Forms.Button borrarIngredientesInsertar;
        private System.Windows.Forms.ListBox listaIngredientesInsertar;
        private System.Windows.Forms.Button anadirIngredientesInsertar;
        private System.Windows.Forms.TextBox ingredientesInsertar;
        private System.Windows.Forms.TextBox nombreReceta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label AñadirReceta;
    }
}

