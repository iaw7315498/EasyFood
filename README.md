# EasyFood
---
*Proyecto iniciado por:*
[**Jefferson Paul Robles Calle**](https://github.com/iaw7315498), 
[**Yunet Labrada Alpizar**](https://gitlab.com/yyunet), 
[**Katherine Mendez**](https://gitlab.com/mendezgalbier), 
[**Daud Saleem**](https://gitlab.com/iperdaud).



### Descripción

+ Programa de escritorio que te permite buscar recetas de cocina filtrando por los _ingrediente_, _utensilios_ ó el _tiempo_.


### Diseño
+ Contiene la página principal donde tendrémos el formulario [desing]
+ Dentro de la página principal tenemos todos los eventos (load, click, focus, etc...)	
+ Tenemos una clase que se encarga de gestionar el acceso a la base de datos
+ Otra clase que se encarga de mostrar los resultados que obtendremos de las consultas a la base de datos

### Técnologias
```sh
Visual estudio
Lenguaje de programación C#
SQL Server
Git
ORM (entity framework)
```
