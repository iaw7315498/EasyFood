USE [master]
GO
/****** Object:  Database [Food]    Script Date: 20/11/2016 11:28:32 ******/
CREATE DATABASE [Food]
GO
ALTER DATABASE [Food] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Food].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Food] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Food] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Food] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Food] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Food] SET ARITHABORT OFF 
GO
ALTER DATABASE [Food] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Food] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Food] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Food] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Food] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Food] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Food] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Food] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Food] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Food] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Food] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Food] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Food] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Food] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Food] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Food] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Food] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Food] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Food] SET  MULTI_USER 
GO
ALTER DATABASE [Food] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Food] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Food] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Food] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Food] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Food] SET QUERY_STORE = OFF
GO
USE [Food]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Food]
GO
/****** Object:  Table [dbo].[Imagenes]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Imagenes](
	[Imagen_id] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Imagenes] PRIMARY KEY CLUSTERED 
(
	[Imagen_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ingredientes]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ingredientes](
	[Ingredientes_id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
 CONSTRAINT [PK_Ingredientes] PRIMARY KEY CLUSTERED 
(
	[Ingredientes_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Recetas]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recetas](
	[Receta_id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Tiempo] [int] NULL,
	[Tipo_id] [int] NULL,
	[Elaboracion] [text] NULL,
	[Favorito] [bit] NULL,
	[Tips] [text] NULL,
 CONSTRAINT [PK_Recetas] PRIMARY KEY CLUSTERED 
(
	[Receta_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RecetasIngredientes]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecetasIngredientes](
	[Receta_id] [int] NOT NULL,
	[Ingredientes_id] [int] NOT NULL,
 CONSTRAINT [PK_RecetasIngredientes] PRIMARY KEY CLUSTERED 
(
	[Receta_id] ASC,
	[Ingredientes_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RecetasUtensilios]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecetasUtensilios](
	[Receta_id] [int] NOT NULL,
	[Utensilios_id] [int] NOT NULL,
 CONSTRAINT [PK_RecetasUtensilios] PRIMARY KEY CLUSTERED 
(
	[Receta_id] ASC,
	[Utensilios_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipos]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipos](
	[Tipo_id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
 CONSTRAINT [PK_Tipos] PRIMARY KEY CLUSTERED 
(
	[Tipo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Utensilios]    Script Date: 20/11/2016 11:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utensilios](
	[Utensilios_id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
 CONSTRAINT [PK_Utensilios] PRIMARY KEY CLUSTERED 
(
	[Utensilios_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Recetas] ADD  CONSTRAINT [DF_Recetas_Favorito]  DEFAULT ((0)) FOR [Favorito]
GO
ALTER TABLE [dbo].[Recetas]  WITH CHECK ADD  CONSTRAINT [FK_Recetas_Tipos] FOREIGN KEY([Tipo_id])
REFERENCES [dbo].[Tipos] ([Tipo_id])
GO
ALTER TABLE [dbo].[Recetas] CHECK CONSTRAINT [FK_Recetas_Tipos]
GO
ALTER TABLE [dbo].[RecetasIngredientes]  WITH CHECK ADD  CONSTRAINT [FK_RecetasIngredientes_Ingredientes] FOREIGN KEY([Ingredientes_id])
REFERENCES [dbo].[Ingredientes] ([Ingredientes_id])
GO
ALTER TABLE [dbo].[RecetasIngredientes] CHECK CONSTRAINT [FK_RecetasIngredientes_Ingredientes]
GO
ALTER TABLE [dbo].[RecetasIngredientes]  WITH CHECK ADD  CONSTRAINT [FK_RecetasIngredientes_Recetas] FOREIGN KEY([Receta_id])
REFERENCES [dbo].[Recetas] ([Receta_id])
GO
ALTER TABLE [dbo].[RecetasIngredientes] CHECK CONSTRAINT [FK_RecetasIngredientes_Recetas]
GO
ALTER TABLE [dbo].[RecetasUtensilios]  WITH CHECK ADD  CONSTRAINT [FK_RecetasUtensilios_Recetas] FOREIGN KEY([Receta_id])
REFERENCES [dbo].[Recetas] ([Receta_id])
GO
ALTER TABLE [dbo].[RecetasUtensilios] CHECK CONSTRAINT [FK_RecetasUtensilios_Recetas]
GO
ALTER TABLE [dbo].[RecetasUtensilios]  WITH CHECK ADD  CONSTRAINT [FK_RecetasUtensilios_Utensilios] FOREIGN KEY([Utensilios_id])
REFERENCES [dbo].[Utensilios] ([Utensilios_id])
GO
ALTER TABLE [dbo].[RecetasUtensilios] CHECK CONSTRAINT [FK_RecetasUtensilios_Utensilios]
GO
USE [master]
GO
ALTER DATABASE [Food] SET  READ_WRITE 
GO


---- FIN CREACION DE BASE DE DATOS Y TABLAS CON SUS RELACIONES ----



use Food
---- Nomencladores---------------

insert into Tipos(Nombre)
values('Entrante'),('Principal'),('Postre');

insert into Utensilios(Nombre)
values('Sarten'),('Cacerola'),('Horno'),('Microondas'),('Mortero'),('Turmix'),('Fogon'),('Bol'),('Plancha');

insert into Ingredientes(Nombre)
values('Carne ternera'),('Carne cerdo'),('Pollo'),('Cebolla'),('Apio'),('Zanahoria'),
('Pimiento verde'),('Pimiento rojo'),('Guisantes'),('Tomate'),('Cerveza'),('Mantequilla'),
('Aceite'),('Patata'),('Queso'),('Pepino'),('Calabazin'),('Olivas'),('Champi�ones'),('Lechuga'),
('Espinaca'),('Huevo'),('Vino blanco'),('Vino tinto'),('Cava'),('Ajo'),('Pasas'),('Miel'),('Bacon'),
('Arroz'),('Lentejas'),('Judias'),('Garbanzos'),('Leche'),('Pasta'),('Banana'),('Chirimoya'),
('Melocot�n'),('Fresas'),('Granada'),('Guayaba'),('Lim�n'),('Mango'),('Manzana'),('Aguacate'),
('Cous-Cous'),('Pan rallado'),('Agua con gas'),('Piment�n'),('Salm�n'),('Oregano'),('Mostaza'),
('Atun'),('Merluza'),('N�scalos'),('Brocoli'),('Guindilla'),('Panceta'),('Crema de leche'),('Gambas'),
('Camar�n'),('Setas de Cardo'),('Escalonias'),('Ajos tiernos'),('Bechamel'),('Pan de molde'),
('Pan redondo'),('Pan'),('Tortilla mexicana'),('Azucar'),('Bacalao'),('Bicarbonato de sodio'),('Bonito'),
('Brandy'),('Caldo de pescado'),('Caldo de pollo'),('Almejas'),('Almendras'),('Calamar'),('Arroz redondo'),
('Canela'),('Cilantro'),('Coco'),('Comino'),('Co�ac'),('Aceite girasol'),('Aceite oliva'),('Azafran'),
('Espaguettis'),('Filete de lenguado'),('Harina'),('Jamon'),('Jamon york'),('Jengibre'),('Leche condensada'),
('Leche Desnatada'),('Maicena'),('Maiz dulce'),('Naranja'),('Palitos de mar'),('Pasta de ajo'),('Pavo'),('Perejil'),
('Pimenton dulce'),('Pimenton picante'),('Pimienta'),('Queso crema'),('Queso Parmesano'),('Queso Filadelfia'),
('Quinoa'),('Rabas'),('Romero'),('Salsa de soja'),('Salsa negra'),('Tabasco'),('Sidra'),('Tahini'),('Tallarines'),
('Tilapia'),('Tomate Ri�on'),('Vainilla'),('Yogurt'),('Zumo de naranja'), ('Placas de lasa�a'),('Sal'),('Lima'),('Gaseosa'),
('Arroz Basmati'),('Jam�n Iberico'),('Chispita de azafr�n'),('Chorizo'),('Ajo en polvo');


---Recetas de carne Victor-----

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Pan erizo',30,2,'
INGREDIENTES
- 1 pan redondo con semillas de 400 gr
- 15 lonchas muy finas bacon
- 2 zanahorias
- 2 cucharas pasas
- 1 cucharas miel
- 15 a 20 lonchas muy finas queso (manchego, comt�...)

UTENSILIOS
- Sart�n
- Horno
- Fog�n

PASOS
1. Cortar el pan en cubitos hasta 1cm de la base. Rallar las zanahorias y cocerlas con un poco de aceite de oliva durante 5 minutos. A�adir las pasas y la miel y dejar cocer un poco m�s. Calentar el horno a 160 grados.
2. Cortar las lonchas de bacon en dos. Separar los cubitos de pan poner las zanahorias con pasas, las lonchas de bacon y las lonchas de queso. Poner el pan sobre un papel de cocci�n y tapar con otro papel. Al horno durante 15 a20 minutos y retirar la tapa y dejar en el horno apagado.',0);


insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Estofado de carne',60,2,'
INGREDIENTES
- 350 gr ternera troceada
- 1 pimiento verde
- 1 kg y medio - 2 Kg patata
- 1 zanahoria
- 1/2 cebolla
- 1/2 vaso vino blanco
- 1 diente ajo
- sal
- aceite
- agua
- 1 pizca azafr�n o colorante alimenticio

UTENSILIOS
- Cacerola
- Fog�n

PASOS
1. Si la carne nos la cort� el carnicero muy grande la cortamos un poco m�s. 
Para mi el tama�o ideal es el que entre en la cuchara junto con un trozo de patata, as� lo vamos comiendo todo junto.
2. Ponemos en la olla un chorrito de aceite y cuando est� caliente a�adimos la carne. La dejamos que se selle bien por todos lados y as� no pierda jugosidad.
3. Mientras tanto troceamos la verdura en cuadraditos (excepto la patata) y la zanahoria en rodajitas. Se lo a�adimos a la carne junto con el vino blanco.
4. Cuando evapore el vino a�adimos un litro de agua aproximadamente y dejamos cocer a fuego medio durante unas dos horas que es lo que tardar� en ablandar la carne 
(hay que tener en cuenta el tama�o de la carne, cuando m�s grande sean los trozos m�s tiempo tardar� en ablandarse, uno de los motivos por lo que yo la corto). 
Hay que vigilar que no se quede sin agua sino se nos quemar�. A�adirle agua tantas veces como ve�is necesario.
5. Cuando ya est� tierna la carne pelamos y cortamos a gajos las patatas y se la a�adimos junto con un poco de azafr�n o colorante alimenticio.
6. Cuando tengamos tierna pero sin deshacerse la patata ya estar� listo para comer.
7. Este plato se puede preparar con diferentes carnes (ternera, cerdo, pollo, conejo...) dependiendo de la carne que escojamos necesitar� m�s o menos tiempo de cocci�n.',0);

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Albondigas',40,2,'
INGREDIENTES
- 800 gr. carne mixta picada (en mi caso baja en Sal y Grasa de Lidl)
- 200 gramos espinaca picada
- 1 huevo batido
- Dos rebanadas Pan de molde mojadas en leche
- Ajo en polvo, cebolla en polvo, pimienta y sal
- 2 vasitos medidores de agua
- 1 chorre�n Vino Blanco
- Perejil picado

UTENSILIOS
- Sart�n
- Cacerola
- Fog�n
- Bol

PASOS
1. Ponemos en un Bol todos los ingredientes de las alb�ndigas, amasamos y formamos bolas que colocamos directamente en la Cecofry pincelada con aceite y precalentada. Las fre�mos durante 10 minutos. Con esta cantidad habr� que hacer dos tandas. Una vez hechas las reservamos.
2. A�adimos los ingredientes de la salsa a la Cubeta de nuestra Olla, removemos un poco para que se mezcle todo y a�adimos las alb�ndigas. Programamos men� Guiso 8 minutos, presi�n media. Cuando acabe, despresurizamos y movemos un poco la cubeta con movimientos circulares para ligarlo todo. ��Ya solo queda buscar pan para mojar!!',1)

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Tacos de ternera',20,2,'
INGREDIENTES
- 350 gramos morcillo de ternera
- 4 tortillas mexicana odelpaso
- lechuga mezclum
- tomate
- champi�ones
- queso gouda
- Pimiento

UTENSILIOS 
- Sart�n
- Fog�n

PASOS
1. Cortamos la carne en tiras al igual que el pimiento, sazonamos con el ajo machacado, or�gano, sal y salsa de soja. En una olla r�pida pondremos un pelin de aceite y echamos la carne, doramos unos minutos, luego cubrimos con vino y cocemos hasta que est� tierna.
2. Calentamos la tortilla pondremos queso, la lechuga, carne y tomate. Servimos con mayonesa y k�tchup.',1);

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Pastel de carne',45,2,'
INGREDIENTES
- 600 gr carne picada de ternera
- 1 cebolla morada
- 2 ramas apio
- 2 zanahorias
- 1/2 pimiento verde
- 1/2 pimiento rojo
- guisantes
- 1 cucharada concentrado de tomate
- 1/2 botella cerveza negrade calidad
- salsa Perrins
- queso chedar rallado
- mantequilla
- sal
- pur� de patatas
- 1 paquete masa brise

UTENSILIOS
- Cacerola
- Horno

PASOS
1. Precalentamos el horno a 180� arriba y abajo sin ventilador.
2. Estendemos la masa brise en un molde engrasado y pinchamos bien el fondo. Lo ponemos en el horno por espaco de unos 15 minutos.
3. Picamos todas las verduras muy peque�as y en unacazuela con mantequilla las pasamos por espacio de 5 minutos,a continuaci�n le ponemos los guisantes y la carne picada y le daremos vueltas hasta que la tengamos casi hecha, le ponemos el concentrado de tomate y le damos unas cuantas vueltas.
4. Le a�adiremos la 1/2 botella de cerveza y dejaremos que se nos consuma el l�quido, a continuaci�n le ponemos un poco de salsa Perris (al gusto), haremos que no nos quede nada de l�quido ya que tenderemos que pasar la carne sobre la masa brise que hay medio horneada.
5. Extendemos bien la carne con las verduras y le ponemos por encima un poco de queso chedar rallado.
6. Hacemos un pur� de patatas bien espeso y tapamos la carne y el queso con �l y le ponemos un poco m�s de queso rallado, lo llevamos al horno hasta que veamos que el queso se ha fundido bien y empieza a dorarse.',0);

-----------Recetas Daud Pasta-------------
insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Espaguetis con camarones y crema',15,1,'

INGREDIENTES
- 1 Chorro de Aceite
- 1 Pizca de Sal
- 280 Gramos de Camar�n
- 1 Unidad de Cebolla Perla
- 1 Unidad de Tomate Ri��n
- 1 Rama de Apio
- 1 Taza de Crema de leche
- 200 Gramos de Espaguetis

UTENSILIOS
- Cazuela
- Sart�n
- Fog�n

PASOS
1. En un sart�n poner a sofre�r la cebolla perla picada en cuadritos con sal al gusto y un poquito de aceite hasta que est� cristalina.
2. Cuando est� a punto de deshacer la cebolla perla, rallar el tomate ri��n que tiene que ser grande, bien rojo y maduro, cernir y verter en el sofrito.
3. A continuaci�n, a�adir los camarones al sofrito, previamente bien lavados y pelados.
4. Una vez los camarones est�n cocidos. a�adir la crema de leche y dejar que hierva hasta que espese. Antes de apagar echar el apio picado y dejar un par de minutos al fuego. Corregir la saz�n si es necesario.
5. Aparte, cocinar los espaguetis, o el tipo de pasta de su preferencia, e forma tradicional con agua hirviendo y sal. Puedes a�adir unas hojas de albahaca al agua de cocci�n.
6. Finalmente servir la pasta y ba�ar con la salsa. Puedes decorar los espaguetis con camarones y crema con unos camarones solos o unas hojas de albahaca. Acompa�a con queso parmesano y un poco de pan casero.
',0);

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Espaguetis a la Mantequilla',30,1,'

INGREDIENTES

- 1 Paquete de Espaguetis
- 5 Unidades de Jitomates medianos maduros
- 1 Unidad de Cebolla
- 2 Cucharaditas de Caldo de pollo en polvo
- 1/4 Tarro de Crema de leche
- 1 Barra de Mantequilla
- 1 Pizca de Sal

UTENSILIOS
- Cazuela
- Sart�n
- Fog�n

PASOS
1. Lo primero que vamos a hacer es cocer la pasta. Para ello, coge una olla, ll�nala de agua hasta la mitad, a�ade una cucharada de mantequilla, un pedazo de cebolla y espera a que hierva. Cuando alcance el punto de ebullici�n incorpora los espaguetis y coc�nalos hasta que queden al dente.
2. Cuando est�n cocidos, esc�rrelos con agua fr�a y res�rvalos para m�s adelante. Para seguir con la preparaci�n de los espaguetis con crema, tritura los jitomates con otro trozo de cebolla y cuela la mezcla para retirar los grumos.
3. Ahora coge una sart�n y pon a calentar dos cucharadas de mantequilla. Cuando se haya derretido, incorpora los espaguetis ya escurridos y sin la cebolla y fr�elos durante tres minutos a fuego medio. Pasado el tiempo, a�ade el jitomate triturado y agrega el caldo de pollo en polvo. Deja que se cocine todo durante cinco minutos. De forma opcional, puedes agregar otros ingredientes como tiras de bacon o champi�ones.
4. Si lo conseideras necesario, a�ade una pizca de sal. Agrega la crema de leche, m�zclalo todo y deja que se cocine hasta empiece a hervir. En el momento en el que alcance el punto de ebullici�n, retira la sart�n del fuego y �listo! Ya puedes servir tus espaguetis en crema con un poco de parmesano espolvoreado y acompa�arlos con unos deliciosos rollitos de pollo empanado, o la carne que m�s te guste.
',0);

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Tallarines con brocoli',30,1,'

INGREDIENTES

� 100/150gr. de tallarines
� 4 ramilletes de brocoli
� 4 n�scalos
� 2 champi�ones
� guindilla cayena
� pimienta negra
� sal
� aceite de oliva
� 1 diente de ajo

UTENSILIOS
- Cazuela
- Sart�n
- Fog�n

PASOS
1. Cocer la pasta siguiendo las indicaciones de tiempo del paquete (cada pasta tiene su tiempo). 
2. Picamos el diente de ajo en rodajas y lo ponemos en una sarten con aceite a fuego lento.
3. A�adimos la cayena. Lavar y partir las setas en l�minas. Ponemos en la sart�n. 
4. Despu�s lavar y partir el br�coli en ramilletes y a�adir a la sart�n. Poner un poco de sal y un toque de pimienta negra.
5. Escurrir la pasta, mezclar con las setas y poner un poco de aceite por encima. Listo para comer.
',0);

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Carbonara',30,1,' 

INGREDIENTES

 - 250 gramos de espaguetis
 - 150 gramos de panceta fresca
 - 150 gramos de queso parmesano
 - 4 huevos grandes
 - 4 dientes de ajo
 - Aceite de oliva virgen extra
 - Sal
 - Pimienta negra molid

 UTENSILIOS
 - Cazuela
 - Sarten
 - Fog�n

 PASOS
1. Comenzaremos preparando la pasta antes de nada, para lo que vamos a poner a calentar una olla con abundante agua, 
y cuando comience a hervir agregamos una pizca de sal y un chorrito de aceite de oliva y los espaguetis,
 y los vamos a dejar cocinar durante unos 12-13 minutos, bajando un poco la intensidad del fuego para que los borbotones 
de agua no se salgan del recipiente al hervir. Pasado ese tiempo los espaguetis deben estar en su punto, 
as� que los vertemos en un escurridor para pasta y dejamos se escurra todo el agua sobrante. 

2. Mientras se cuecen los espaguetis podemos aprovechar para preparar el resto de ingredientes que vamos a necesitar en la 
receta. Pelamos los dientes de ajo y los cortamos en rodajas que sean m�s bien finas. Cortaremos tambi�n la panceta en 
tiras, a menos que ya las hayamos comprado as�, aunque a nosotros nos gusta m�s comprarlas en lonchas y luego cortarlas, 
pero compra la que prefieras. Y respecto a los huevos, es mejor que no est�n fr�os de la nevera, as� que es interesante 
dejarlos fuera de la misma cuando empecemos a preparar esta receta. 

3. A continuaci�n ponemos a calentar una sart�n de tama�o adecuado con un poco de aceite de oliva virgen extra,
 para dorar en ella los ajos laminados, mejor a fuego medio para evitar que se quemen. Una vez los ajos est�n bien 
salteados por todos lados, los sacamos de la sart�n y dejamos reservado para despu�s. En esa misma sart�n vamos a saltear 
la panceta que hab�amos cortado antes, hasta que quede levemente frita por todos lados, pero sin que llegue a quedar 
frita del todo y quede crujiente. Cuando est� lista, apagamos el fuego y apartamos la panceta junto al ajo que hab�amos 
salteado antes. 

4. Lo siguiente que haremos ser� batir los huevos que ten�amos fuera de la nevera. Podemos hacerlo batiendo los huevos 
enteros o bien usando solo las yemas de los mismos, que es como se hace en algunas zonas de Italia de forma tradicional. 
Haz la opci�n que m�s te guste, pero ten en cuenta que esta segunda opci�n hace que el sabor a huevo sea m�s intenso a la 
hora de servir el plato de espaguetis a la carbonara. Una vez tengamos los huevos batidos, o sus yemas, agregamos el 
queso parmesano rallado y sal y pimienta negra molida al gusto, y volvemos a batir para mezclar bien. 

5. Y ahora vamos a acabar de preparar el plato, para lo que echamos en la sart�n que hab�amos usado antes los espaguetis 
bien escurridos, junto a la panceta y los ajos, y lo salteamos todo a fuego medio durante un par de minutos, 
para que la pasta tome algo de sabor y los ingredientes se calienten bien. Servimos a continuaci�n en los platos 
individuales, y por encima agregamos parte de la mezcla de huevos batidos y queso que hab�amos preparado, 
para que con el calor de la pasta el huevo no se cuaje del todo y quede cremoso.
',0);


insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Falso ravioli de gambas, setas y ajetes',30,1,'

INGREDIENTES

- 12 placas de Lasa�a Directa al Horno Gallo 
- 500g de gambas crudas peladas 
- 400g de setas de cardo 
- 6 ajetes tiernos 
- 2 cucharadas de aceite 
- 2 escalonias  
- Sal 
- Pimienta 
- 600 ml de bechamel 

 UTENSIILIOS
- Horno
- Sart�n
- Fog�n

PASOS
1. Limpia y pica los ajetes. Trocea las setas. Pela y pica o ralla las escalonias. 
2. Para el relleno, pon en una sart�n el aceite a fuego medio, incorpora los ajetes picados y las escalonias ralladas, saltea unos minutos sin que tomen color y a�ade entonces las setas y cocina dos minutos m�s. 
3. Agrega las colas de gamba crudas y apaga el fuego. Pon al punto de sal y pimienta y reserva aparte. 
4. Para el montaje, pon a remojar las placas de lasa�a en un recipiente lleno de agua tibia hasta que est�n blandas. 
5. Haz los raviolis depositando una buena cucharada de relleno sobre la mitad de cada placa de lasa�a. Dobla por la mitad la pasta tapando el relleno y formando as� el ravioli.  
6. Coloca los raviolis terminados sobre una bandeja de horno y c�brelos completamente con la bechamel. 
7. Precalienta el horno a 200�, y coloca la fuente con los raviolis en el nivel del medio durante 15 minutos (al final puedes encender el grill para gratinar m�s la superficie). 
',0);

----- Recetas Yunet Postres -----------
insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Flan de coco',25,3,'

INGREDIENTES
- 1 bote grande leche condensada 
- 5 huevos 
- 2 medidas del bote de la leche condensada leche 
- Az�car para caramelizar el molde 
- 575 g coco rallado.

UTENSILIOS
- Fogon
- Caserola
- Bol

PASOS
1. En una sart�n antiadherente se hace caramelo el az�car (unos 100 g aprox) a fuego suave, moviendo la sart�n para que se dore por igual. Se vierte en el molde y r�pidamente se gira para que se empapen bien las paredes del molde.
2. En un bol grande se ponen las cinco yemas separadas de las claras. Se agrega a las yemas el bote de leche condensada, mas el coco rallado,mezclando poco a poco con una cuchara de madera. Se llena de leche el bote vac�o de la leche condensada y se vierte sobre la mezcla de yemas poco a poco. Se repite la operaci�n otra vez.
3. Se montan las claras a punto de nieve fuerte, a�adiendo una pizca de sal para que queden mas duras. Se agregan a la mezcla uni�ndolas con movimientos envolventes con ayuda de una esp�tula de goma. La mezcla quedara espumosa. Con ayuda de un cazo se llena el molde.
4. Se mete el molde en una cazuela con dos dedos de agua, se pone a cocer a fuego suave y se tapa. La tapadera se cubrir� con un pa�o de felpa sujeto al asa con una pinza, para evitar que el vapor caiga sobre el flan. Roto el hervor, tardara unos veinte minutos aprox. en hacerse
 
',0)

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Capuchino',35,3,'

INGREDIENTES

- 10 yemas de huevo
- 1 clara de huevo
- 2 cucharadas de az�car blanca
- 4 cucharaditas de maicena
Alm�bar
- 3 tazas de az�car blanca
- 1/2 taza de agua
- 1 lim�n
- 1 palo de canela
- 1 cucharadita de vainilla

UTENSILIOS
- Bol
- Turmix
- Horno

PASOS
1. Encienda el horno a 375 grados F. Prepare unos cucuruchos con papel para hornear, los cucuruchos deben tener un largo de 10 cent�metros mas o menos y coloquemos en un molde para capuchinos que consiste en un molde con agujeros para colocar los cucuruchos verticalmente en cada agujero para posteriormente rellenarlos. Paso 2. Bata las yemas, la clara y el az�car durante 15 minutos hasta que este bien espeso; a�adale poco a poco la maicena cernida, removiendo todo suavemente; viertalo en una manga con boquilla mediana y llene los cucuruchos hasta las dos terceras partes. Horneelos alrededor de 15 minutos o hasta que est�n doraditos en la parte superior. Para evitar que se quemen las puntas de los cucuruchos, ponga una tartera llana con agua en la parrilla inferior del horno, mientras hornea los capuchinos en la parrilla superior. Despu�s de horneados, quiteles el papel y ba�elos con alm�bar. Salen 20 capuchinos aproximadamente.
2. Preparaci�n del alm�bar
Ponga al fuego el az�car, el agua, la canela, la cascara de lim�n y unas gotas de su zumo. Dejarlo hervir durante 3 minutos. A�ada la vainilla. Dejarlo refrescar antes de a�adirlo a los capuchinos que deben quedar bien mojados en la alm�bar.
',0)

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values('Tarta de queso y yogurt',50,3,'

INGREDIENTES
 
- 4 yogures naturales azucarados (125g cada uno) 
- 4 huevos L 
- 250 g queso tipo philadelphia 
- 12 cucharadas soperas az�car 
- 4 cucharadas maicena

UTENSILIOS
- Turmix
- Bol
- Horno

PASOS
1. En un recipiente se a�aden todos los ingredientes y se baten con la batidora.
2. Modo horno: Para hacerla en horno precalentado a 180�C durante 35-45 minutos o hasta que el palillo est� limpio, depende del horno. Le pod�is a�adir mermelada de higos que combina muy bien con esta tarta.
',0)

---- Recetas Monica Vegetales-------------------------------------------------------

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Hummus', 10, 1, '

INGREDIENTES
- 2 dientes de ajo
- 1 bote garbanzos cocidoa
- 2 cucharadas tahini
- Aceite de oliva
- 1 limon
- Comino
- Sal
- Piment�n dulce

UTENSILIOS
- Turmix

PASOS
1. Lavar garbanzos. 
2. Poner los garbanzos, dos dientes de ajo, tahini,aceite de oliva, comino y zumo de un limon en el vaso. 
3. Triturar bien todo. 
4. A�adir aceite de oliva al gusto en funci�n de la cremosidad que quieras obtener. 
5. Poner en un bol. Espolvorear piment�n y un chorrito de aceite de oliva.', 
0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Guacamole', 15, 1, '

INGREDIENTES
- 5 Aguacates
- Zumo de 2 limas 
- 1 Cebolla
- 2 Tomates
- 1 Diente de ajo
- 1 Gaseosa
- Sal

UTENSILIOS
- Mortero

PASOS
1. Pelar y machacar los aguacates en un bol con la ayuda del mortero.
2. A�adir el zumo de lima. 
3. Picar muy bien la cebolla, el tomate y el ajo y a�adir.
4. Una vez que est� la mezcla terminada a�adir un poco de agua con gas, remover y dejar reposar 5 minutos. 
5. Servir en un bol y acompa�ar con totopos', 
0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Hamburguesas de cous-cous y garbanzos', 25, 2, '

INGREDIENTES
- 100 gramos cous-cous
- Medio bote garbanzos cocidos
- Pan rallado
- Especias al gusto
- Sal

UTENSILIOS
- Turmix

PASOS
1. Poner en un recipiente dos veces m�s agua muy caliente que cous-cous y tapar. 
2. Dejar reposar 10 minutos. 
3. Mientrasmachacar los garbanzos en un bol con un tenedor. 
4. Mezclar bien el cous-cous con los garbanzos y el pan ralllado.
5. Amasar.
6. Dejar reposar la masa en la nevera 
7. Calentamos aceite en la sart�n. 
8. Hacer hamburguesas con la masa y freir a fuego medio hasta que se queden crujientes',
0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Falafel', 40, 2, '

INGREDIENTES
- 200 gramos garbanzos en remojo
- Media cebolla
- 1 Zanahoria
- 1 Diente de ajo
- Perejil fresco
- Especias al gusto (Comino recomendado)
- Aceite de oliva
- Pan rallado
- Sal

UTENSILIOS
- Turmix

PASOS
1. Poner los garbanzos en remojo al menos 5 horas. 
2. Cortar media cebolla, una zanahoria, un diente de ajo y perejil fresco. 
3. Mezclar en el vaso de la turmix todos los ingredientes y batir hasta que se quede una mas. 
4. A�adir pan rallado hasta conseguir una masa consistente. 
5. Dejar reposar la masa en la nevera mientras calentamos aceite en una sart�n. 
6. Hacer peque�as hamburguesas y freir.
7. Servir con tu salsa favorita'
, 0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Pisto', 40, 2, '

INGREDIENTES
- 3 Cebollas
- 3 Calabacines
- 6 Tomates
- Aceite de oliva
- 100 gramos de arroz
- Sal

UTENSILIOS
- Sarten
- Fogon

PASOS
1. Cortar tres cebollas, tres calabacines y seis tomates en juliana. 
2. Poner aceite de oliva en una sarten grande, a�adir un diente de ajo bien picado.
3. Cuando est� dorado a�adir la cebolla, dejar 10 minutos.
4. A�adir el calabac�n, dejar 10 minutos 
5. A�adir la cebolla. 
6. Salpimentar. 
7. Cocer a fuergo lento durante 20 minutos.
8. Mientras se hace el pisto cocer arroz. 
9. Presentar en un plato un poco de arroz y pisto.'
, 0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Espinacas con garbanzos', 15, 2, '

INGREDIENTES
- 1 Bote garbanzos cocidos
- 300 gramos de espinacas
- 2 Dientes de ajo
- Aceite de oliva
- Sal

UTENISLIOS
- Sarten
- Fogon

PASOS
1. Cortar dos dientes de ajo. 
2. Poner aceite de oliva en la sarten y a�adir los ajos.
3. A�adir un bote de garbanzos cocidos y especias al gusto. 
4. Dejar durante 5 minutos.
5. A�adir las espinacas y dejar de que hagan con los garbanzos.'
, 0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Tomates rellenos de quinoa', 50, 2, '

INGREDIENTES
- 80 gramos de quinoa
- 2 Tomates
- Aceite de oliva
- Sal
- Pimienta
- Media cebolla
- Medio litro caldo de verduras

UTENSILIOS
- Horno
- Sarten

PASOS
1. Quitar la parte de arriba de 3 tomates 
2. Vaciar y poner en una bandeja de horno con aceite, sal y pimienta. 
3. Reservar el interior del tomate. 
4. Poner a cocer la quinoa durante 10 minutos en caldo de verduras. 
5. Poner aceite en una sart�n, a�adir un diente de ajo bien picado, un poco de cebolla y sofreir. 
6. A�adir la quinoa y el tomate y sofreir durante 10 minutos. 
7. Sacar los tomates del horo y rellenar con el guiso y servir.'
, 0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Crema de zanahoria y patata', 50, 2, '

INGREDIENTES
- 1 Cebolla
- 6 Zanahorias
- 2 Patatas
- Ajo
- Sal
- Pimienta

UTENSILIOS
- Caserola
- Turmix

PASOS
1. Cortar la cebolla.
2. Pelar las zanahorias y patatas y cortar.
3. Poner en una cacerola un poco de aceite. 
4. A�adir un diente de ajo picado y el resto de ingredientes. 
5. Sofreir hasta que la cebolla se poche. 
6. A�adir medio litro de agua, sal y pimienta y dejar cocer durante al menos 25 minutos.
7. Quitar el agus restante y guardar. 
8. Con la turmix triturar y a�adir agua en funci�n de la cremosidad que prefieras.
9. Servir en un plato. 
10. Puedes usar el caldo para la elaboraci�n de otros platos.'
, 0);

insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion, Favorito)
values ('Patatas crujientes al horno', 35, 1, '

INGREDIENTES
- 5 Patatas
- 1 Cucharada de harina
- Sal
- Especias al gusto

UTENSILIOS
- Horno
- Caserola

PASOS
1. Cortamos las patatas. 
2. Colocamos las patatas con piel en una cacerola, y a�adimos agua fr�a hasta que cubra las patatas. 
3. Calentamos la cacerola con las patatas, y cuando el agua rompa a hervir, calculamos 5 minutos.
4. Ponemos el horno a precalentar a 200�C. 
5. Pasados esos 5 minutos, sacamos las patatas del agua, y secamos un poco, y las devolvemos a la cacerola (sin agua). 
6. A�adimos a las patatas una cucharada de harina integral, sal, y especias al gusto (por ejemplo romero, piment�n, ajo y cebolla en polvo). 
7. Tapamos la cacerola, y agitamos bien varias veces para que las patatas se empapen de la mezcla de harina y especias. 
8. Colocamos las patatas sobre una bandeja de horno con papel de horno para que no se nos peguen, y las metemos 
al horno durante unos 20 minutos. 
9. Si las queremos m�s doradas, podemos subir la temperatura y dejarlas otros 5 minutos en el horno. 
10. Pod�is tomarlas tal cual, o acompa�arlas de vuestras salsas preferidas.'
, 0);


--- Recetas Jefferson Arroz----
insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion) 
values ('Arroz chino varias delicias', 25, 1, '

INGREDIENTES
- 2 dientes de ajo
- 1 cebolla
- 100gr guizantes
- 3 huevo 
- sal
- 1 zanahoria
- 50gr jam�n york
- 50gr ma�z
- 1 chorro salsa de soja

UTENSILIOS
- Cazuela
- sarten

PASOS
1. Rehogar 2 dientes de ajo en una cazuela. A�adir la cebolla y luego los guisantes. 
2. Incorporar el arroz y el agua para cocer todo durante 12 min. A�adir sal. 
3. Una vez cocido, escurrir y reservar. Cocer 1 huevo y picarlo. Reservamos.
4. Batir los otros dos huevos y hacer una tortilla francesa. Partirla en trocitos. Reservamos. 
5. Con un pela patatas, hacer tiras de zanahoria y trocearlas. 
6. En una sart�n ponemos un poco de aceite y salteamos la zanahoria, el jam�n de york que habremos hecho en cuadratidos y el ma�z. 
7. Incorporamos el huevo cocido y la tortilla. 
8. Por �ltimo a�adimos el arroz escurrido en la sart�n. Salteamos con un chorro de salsa de soja. A�adir sal al gusto.');


insert into Recetas (Nombre, Tiempo, Tipo_id,  Elaboracion) 
values ('Pimientos rellenos de arroz con verduras', 25, 1, '

INGREDIENTES
- 2 pimientos rojos 
- 500 gr arroz 
- 50 gr ma�z dulce 
- 1 zanahorias 
- 50 gr guisantes 
- 2 ajos 
- aceite de oliva 
- 1 pastilla caldo concentrado 
- sal 

UTENSILIOS
- Cazuela
- Bol
- Horno

PASOS
1. Cortar la parte de arriba de los pimientos y quitarle las semillas. 
2. Lavar los pimientos y meterlos en el microondas con un poco de sal durante 3/4 minutos. 
3. Cocer el arroz durante 15/17 minutos con la pastilla de caldo concentrado y escurrir. 
4. Pelar y cortar la zanahoria a daditos y cocer con un poco de sal durante unos 4/5 minutos, y escurrir. 
5. Abrir una lata de guisantes y otra de ma�z dulce, escurrirlos y enjuagarlos. 
6. Rallar un poco de queso. 
7. Pelar los ajos dejarlos enteros y fre�rlos en aceite de oliva y cuando est�n fritos los apart�is. 
8. En un bol grande mezclar el arroz con una cucharada de aceite de haber frito los ajos, los guisantes, la zanahoria, el queso rallado y el ma�z e ir rellenando los pimientos, taparlos con la tapa que le quitamos al pimiento al principio. 
9. Poner en una fuente de horno y meterlos dentro del horno durante unos 8/10 minutos a 150/180 � y servir. 
10. Otra opci�n es a�adirles trocitos de jam�n a la mezcla, por si no os gusta con queso.');


insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion) 
values ('Arroz especial', 25, 1, '

INGREDIENTES
- 1 taza arroz basmati
- 1/2 bandeja champi�ones 
- Pu�ado guisantes 
- Pu�ado ma�z dulce
- 1/2 cebolla trozeada
- 3 lonchas pavo 
- Chorro salsa de soja
- 1 huevo

UTENSILIOS
- Cazuela
- Sarten
- Fogon

PASOS
1. Cocinamos el arroz en agua hirviendo 12 minutos exactos. Colamos y reservamos. 
2. En una sart�n ponemos un poco de aceite o mantequilla. Echamos los champi�ones, la cebolla, los guisantes y el ma�z. Cocinamos unos minutos. 
3. Cuando est� a�adimos el pavo y la salsa de soja. A�adimos justo despues el arroz. Movemos bien. 
4. Por �ltimo a�adimos el huevo batido y removemos bien durante unos minutos para que el huevo se haga. No dejamos de remover porque si no el arroz puede pegarse a la sart�n.');


insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion) 
values ('Arroz frito chino', 30, 1, '

INGREDIENTES
- 2 huevos hechos en tortilla francesa
- 3 filetes cerdo troceados peque�o
- 1 zanahoria rallada
- 1 pu�ado guisantes cocidos
- 1 vaso arroz cocido
- 1 cebolla mediana picadita
- palitos de mar
- 100 gramos trocitos de jam�n ib�rico
- aceite de girasol
- 1 diente ajo

UTENSILIOS
- Caserola
- Fogon

PASOS
1. Poner agua a hervir y cocer el pu�ado de guisantes. En otra cazuela cocer el arroz. 
2. Mientras se hacen los guisantes freimos los huevos y rallamos la zanahoria. 
3. En una cacerola salteamos la cebolla y el ajo, luego a�adimos los trocitos de lomo. Una vez doraditos agregamos el jam�n y la zanahoria rallada. Un pelin de sal vendr�a bien en este momento. 
4. Agregamos los palitos de mar cortados en tres cada uno. Luego agregamos el arroz, guisantes y huevos cortado l�gicamente en trocitos, sofre�mos todo durante unos minutos, a�adiremos la salsa de soja. Dejaremos unos minutos m�s al fuego y listo.');


insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion) 
values ('Arroz caldoso con mariscos y verduras', 35, 1, '

INGREDIENTES
- 100 gramos arroz redondo
- 2 zanahorias
- 1 patata
- 1/2 cebolla
- 2 dientes ajo peque�os machacados
- 650 ml caldo de pescado
- 1 chispita de azafr�n
- sal al gusto
- 6 gambones
- 8 rodajas calamares
- 1 filetito de merluza

UTENSILIOS
- Caserola
- Fogon

PASOS
1. En una cacerola pondremos el caldo de pescado y la chispita de azafr�n, la cebolla cortada trozos gordos en juliana, los ajitos machacaditos y la zanahoria en tiras gruesas. Tambi�n a�adimos la merluza y las rodajas de calamares. 
2. Cuando rompa a hervir agregamos el arroz y la patata cortada en trozos medianos y retificamos sal, si quereis pod�is a�adir media pastilla de caldo de pescado. Una vez cocido el arroz a�adimos los gambones. Dejamos cocer.');


insert into Recetas (Nombre, Tiempo, Tipo_id, Elaboracion) 
values ('Arroz a la cubana', 25, 1, '

INGREDIENTES
- 1/2 vaso Arroz
- 2 vasos Tomate Triturado
- 1 vaso agua o caldo
- 1/2 Cebolla
- 3 Dientes Ajos
- 50 gramos Chorizo
- 2 Huevos
- Sal
- Or�gano
- Aceite

UTENSILIOS
- Caserola
- Sarten
- Fogon

PASOS
1. Empezamos cortando la cebolla peque�a y fri�ndola en un par de cucharadas de aceite, hasta que est� bien hecha. 
2. Mientras la cebolla se fr�e, cortamos el chorizo a taquitos y picamos el ajo. 
3. Cuando la cebolla est� a vuestro gusto, a�adimos el ajo y el chorizo. Lo dejamos unos 5 minutos a fuego lento, mientras el chorizo suelta su aceite y toda la mezcla se impregna bien. 
4. A continuaci�n incorporamos el tomate triturado y el agua. Si ten�is caldo a mano, pod�is tirarlo para sustituir al agua, y darle otra capa de sabor al sofrito. Removemos bien y dejamos que se reduzca poco a poco. 
5. Mientras el sofrito se reduce, prepararemos los huevos para cocerlos al estilo huevo flor. Para realizar este paso os dirijo a la receta de Huevo Flor de Juli�n Cabello, ya que es la que sigo yo para realizar este paso. Huevo flor o flor de huevo. 
6. Removemos el sofrito cada 5 minutos, e incorporamos el or�gano y la sal al gusto. 
7. Cuando el agua se haya evaporado por completo (lo sabremos porque habr� espesado mucho y ser� posible ver el fondo del cazo al remover), bajamos el fuego al m�nimo y acabamos de preparar las otras partes del plato. 
8. Ponemos 2 vasos de agua en un cazo, y a�adimos el arroz cuando hierva, manteni�ndolo unos 10-12 minutos. 
9. En otro cazo con agua hirviendo, llevamos a cabo el �ltimo paso de la receta Huevo Flor, hirviendo el huevo entre 5-6 minutos para que la yema quede tierna. 
10. Finalmente montamos el plato: primero una capa de arroz, a continuaci�n la mitad del sofrito y finalmente un huevo cocido.');



---Recetas Cristian-----

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Lenguado a la naranja', 45, 2, '

INGREDIENTES
- 4 Unidades de Filete de Lenguado
- 1 Cucharada sopera de Mantequilla
- 2 Tazas de Zumo de naranja
- 120 Gramos de Cebolla larga
- 1 Pizca de Pasta de ajo
- 80 Gramos de Almendras
- 1 Cucharada postre de Maicena
- 1 Copa de Vino blanco
- 1 Pizca de Sal
- 1 Pizca de Pimienta

UTENSILIOS
- Cazerola
- Sart�n
- Fog�n

PASOS
1. Realizar la salsa de naranja para nuestra receta de ni�os. Para ello, en una olla adicionamos el vino blanco, el zumo de naranja, la cebolla larga finamente picada, sal, pimienta y un poco de pasta de ajo. Esto lo llevamos a fuego medio hasta que reduzca a la mitad y se concentren los sabores de la preparaci�n, bajamos a fuego lento y adicionamos la maicena hidratada en un poco de agua para que nuestra salsa se espese. Reservar.Aparte, lleva un sart�n a fuego medio y adiciona la mantequilla. Una vez �sta se derrita agrega los filetes de lenguado.Agrega un poco de sal, pimienta y ajo en pasta para reforzar el sabor de los filetes.
2. Una vez los filetes de lenguado se doren por lado y lado adiciona la salsa de naranja, previamente pasado por un colador. Deja reducir un poco m�s la salsa y apaga el fuego.
Sirve el lenguado en salsa de naranja y decora con almendras enteras o fileteadas, tambi�n puedes a�adir un poco de perejil u or�gano. Esta receta para ni�os es sana, nutritiva y deliciosa y para hacerla m�s apetecible a�n puedes acompa�arla con un pur� de patatas', 0)

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Bu�uelos de Bacalao', 30, 2, '

INGREDIENTES
- 200 Gramos de Bacalao desalado
- 125 Gramos de Mantequilla
- 50 Gramos de Cilantro
- 1 Unidad de Huevo
- 1 Unidad de Cebolla blanca
- 100 Mililitros de Agua
- 200 Gramos de Harina

UTENSILIOS
- Cazerola
- Bol
- Fog�n

PASOS
1. En un olla dorar la cebolla cortada e cuadros.Picar el bacalao en peque�os trozos, revisar que no tenga espinas.Agregar el bacalao a la cebolla y el cilantro picado.
2. Derretir la mantequilla y agregar la harina de golpe, ver que se forme una pasta.
3. Agregar el agua y mezclar.
4. Agregar el huevo y mezclar hasta obtener una masa uniforme m�s o menos homog�nea.
5. En una olla fre�r con aceite bien caliente. Para ellos lanza con cuidado al aceite cucharadas de la masa de los bu�uelos.
6. Servir los deliciosos bu�uelos de bacalao y degustar con queso crema o con un dip de queso crema y albahaca.', 0)


insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Bacalao a la llauna', 30, 2, '

INGREDIENTES
- 3 Filetes de Bacalao desalado
- 2 Dientes de Ajo
- 7 Gramos de Piment�n dulce
- 200 Mililitros de Aceite de oliva virgen extra
- 4 Cucharadas soperas de Harina
- 1/4 Vaso de Vino blanco
- 1 Pizca de Perejil
- 1 Pizca de Sal

UTENSILIOS
- Cazerola
- Sart�n
- Fog�n
- Horno

PASOS
1. El pescado ya lo hemos adquirido desalado. El perejil preferiblemente deber�a ser fresco, pero si no tienes puedes preparar el plato con perejil picado. Lo fundamental es el ajo y el piment�n dulce.
2. En primer lugar enharinamos los lomos de bacalao en un plato, para proceder a fre�rlos a continuaci�n en una sart�n con aceite muy caliente.
Truco: Precalienta el horno a 190 grados.
3. Una vez tengamos el bacalao ligeramente frito, disponemos los filetes en una cacerola para horno. Lo ideal es que sea de hojalata ("llauna" en catal�n, de ah� el nombre de la receta), pero a falta de �l puedes emplear una de barro, por ejemplo.
A continuaci�n preparamos el caldo que ali�ar� el bacalao. En la misma sart�n u olla donde hemos hecho el bacalao, utilizando el mismo aceite, vertemos un diente de ajo. Cuando haya cogido color, a�adimos el vino blanco y el piment�n dulce, y lo dejamos fre�r durante unos minutos. Luego apagamos el fuego y vertemos este caldo sobre el bacalao.A�adimos un poco de perejil, una pizca de sal y horneamos el bacalao a la llauna a 190 grados durante unos 10 minutos.Sacamos el bacalao del horno y ya lo tenemos listo para servir.
Emplatamos la receta de bacalao, espolvoreando por encima un poco m�s de perejil y el otro diente de ajo picado, y ya est� listo para degustar.', 0)



insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Filete de pescado al jengibre', 15, 2, '

INGREDIENTES
- 3 Filetes de Bacalao desalado
- 2 Dientes de Ajo
- 7 Gramos de Piment�n dulce
- 200 Mililitros de Aceite de oliva virgen extra
- 4 Cucharadas soperas de Harina
- 1/4 Vaso de Vino blanco
- 1 Pizca de Perejil
- 1 Pizca de Sal

UTENSILIOS
- Sart�n
- Fog�n

PASOS
1. En una sart�n, derretir la mantequilla a fuego bajo, agregar el ajo y el jengibre finamente picado. Cocinar durante 5 minutos, revolviendo constantemente.A�adir el zumo del lim�n, salpimentar y cocinar a fuego bajo durante 2 minutos m�s. Reservar la salsa.Aparte, en una sart�n con aceite, cocinar los filetes de pescado salpimentados. Cocina durante 3 minutos por cada lado hasta que est�n dorados.Finalmente servir el pescado, cubrir con la mantequilla de jengibre y ajo y buen provecho. El filete de pescado al jengibre se puede acompa�ar con un pur� de papas o unas patatas asadas.', 0)


insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Bonito a la plancha', 15, 2, '

INGREDIENTES
- 150 Gramos de Bonito
- 1 Chorro de Aceite
- 1 Pizca de Sal
- 1/2 Unidad de Lim�n
- 1 Pizca de Romero

UTENSILIOS
- Plancha

PASOS
1. Para aromatizar el pescado hemos optado por un poco de romero y lim�n, aunque tambi�n lo podr�as sustituir por una vinagreta de aceite y vinagre bals�mico o un poco de miel, por ejemplo.
Truco: Pide en la pescader�a que te preparen los lomos de bonito, que te los limpien y que te quiten las espinas. Si lo prefieres, tambi�n puedes pedir el bonito cortado en rodajas.
2. Primero salamos el pescado crudo y lo marinamos con el zumo del medio lim�n. Mientras, echamos un poco de aceite en una plancha y esperamos a que se caliente.
Extendemos los trozos de filete de bonito sobre la plancha, y cuando la capa externa haya cogido color bajamos el fuego para que se cocine la parte interior. D�ndole la vuelta de vez en cuando, no necesitaremos m�s de diez minutos para que est� listo, siempre dependiendo del grosor de los trozos.Cuando el bonito est� listo, lo emplatamos y lo regaremos con un poco m�s de zumo de lim�n y romero. Como acompa�amiento para el bonito a la plancha, te aconsejamos unos tomates maduros frescos para que el plato siga siendo sano, ligero y rico. Aunque siempre uedes optar por un pur� de patatas o un arroz blanco. �Buen provecho!', 0)



insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Calamares a la provenzal', 45, 2, '

INGREDIENTES
- 500 Gramos de Anillos de calamar o rabas
- 1 Taza de Leche tibia
- 1 Cucharadita de Bicarbonato de sodio
- 2 Huevos
- Harina leudante
- Sal
- Pimienta negra
- 2 Dientes de Ajo
- Ajo en polvo (opcional)
- Perejil fresco

UTENSILIOS
- Bol
- Sart�n
- Fog�n

PASOS
1. Poner los calamares en un recipiente, en mi caso ya est�n cortados en anillos, con la taza de leche y la cucharadita de bicarbonato de sodio, y dejarlo as� por 1 hora (no necesita ir a heladera y no es necesario taparlo). Este procedimiento de poner las rabas en leche y bicarbonato se hace para queden tiernas una vez rebozadas.
Truco: Si tienes los calamares enteros tendr�s que limpiarlos bien cortarlos en anillos.
En otro recipiente batir ligeramente los 2 huevos, agregarle sal, pimienta negra y ajo en polvo ( opcional). En otro plato poner un poco de harina leudante (para enharinar las rabas) y en otro preparar la provenzal con perejil picado y 1 o 2 dientes de ajo picado.Cuando haya pasado la hora, limpiar y colar los calamares, lavarlos con agua para sacarle el bicarbonato y la leche. Luego poner a calentar una buena cantidad de aceite a fuego medio.Pasar los calamares por huevo, en mi caso lo hice por tandas de poca cantidad, as� las rabas a la provenzal quedar�n todas bien enharinadas.Luego pasar los calamares por harina leudante, pero enharinar solo los que en ese momento van a ir al aceite, no dejarlos mucho tiempo en la harina porque se humedecen y despu�s los calamares a la provenzal no quedar�n crocantes.Fre�r las rabas a en el aceite caliente durante 3 o 4 minutos, no m�s... realmente controlar el tiempo porque si las dejan m�s de 4 minutos luego quedan duras.Pasado ese tiempo, retirar los calamares fritos y ponerlos en papel absorbente para escurrir el exceso de aceite.Al terminar de fre�r todos, agregar la provenzal a las rabas y �listo!. Ya podr�n disfrutar de sus calamares a la provenzal en cualquier ocasi�n que se precie.', 0)



insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Merluza a la sidra', 45, 2, '

INGREDIENTES
- 6 Rodajas de Merluza
- 1 Unidad de Cebolla grande
- 500 Gramos de Tomates
- 12 Unidades de Almejas
- 4 Unidades de Patatas
- 2 Unidades de Dientes de ajo
- 1 Cucharada postre de Piment�n picante
- 500 Mililitros de Sidra natural (La de escanciar, no confundir con la espumosa)
- 3 Cucharadas soperas de Co�ac o Brandy
- Harina para rebozar
- 1 Pizca de Sal

UTENSILIOS
- Sart�n
- Fog�n
- Bol

PASOS
1. Cortaremos las patatas en rodajas ni finas ni gruesas, a�adimos sal al gusto y fre�mos despacio, hasta que empiecen a dorarse. Cuando est�n listas, las vamos colocando repartidas por la base de la fuente de manera que cubramos todo lo posible la superficie de la misma. Mientras se fr�en las patatas, podemos aprovechar para ir preparando los ingredientes de la salsa con sidra.
Limpiamos las rodajas de merluza, las pasamos por harina y cocinamos en aceite bien caliente hasta que se empiecen a dorar por ambos lados. Seg�n vayan cocin�ndose las piezas de merluza, las vamos colocando sobre la base de patatas que tenemos lista en la fuente. Vamos con lo "goloso" de la receta, la salsa.Cogemos la cebolla, las cabezas de ajo y los tomates, los limpiamos bien y picamos todo en trozos peque�os. Acordaos de quitar el coraz�n de los ajos. Para preparar la salsa necesitaremos 2 o 3 cucharadas de aceite usado anteriormente para fre�r la merluza (lo suficiente para que cubra la base de la sart�n). Antes de usarlo, debemos colarlo para limpiarlo de los restos de harina que hayan podido quedar.
Ponemos el aceite en una sart�n grande y cuando est� caliente a�adimos la cebolla y el ajo y lo dejamos pochar a fuego medio. Una vez tengamos la cebolla pochadita, le a�adimos el tomate, el piment�n, salamos y mezclamos todo bien. Luego, vertemos el co�ac o brandy y la sidra, removemos de nuevo hasta que se nos hayan mezclado bien los ingredientes y dejamos cocinar a fuego medio durante 15 minutos aproximadamente, removiendo de vez en cuando.
�Ya tenemos la salsa lista! Solo nos queda pasarla por el pasapur�s o por la batidora con la finalidad de obtener una preparaci�n m�s fina y sin grumos. Precalentamos el horno a 200�C. Repartimos las 12 almejas por la fuente y a�adimos toda la salsa por encima, procurando que cubra bien todos los trozos del pescado. Metemos la fuente con la merluza en el horno cuando est� caliente y horneamos durante 10 minutos, manteniendo la temperatura indicada.Pasados los 10 minutos en el horno, la merluza a la sidra estar� lista servir.', 0)

insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Merluza rebozada', 15, 2, '

INGREDIENTES
- 1/2 kg Merluza
- 2 cucharadas de sal
- 1 litro aceite
- 100 gr harina
- 2 huevo.

UTENSILIOS
- Bol
- Sart�n
- Fog�n

PASOS
1. Se sala la merluza y se reboza (pasar por harina y luego huevo).
2. Se fr�e en aceite bien caliente.', 0)


insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Tartar de at�n picante', 45, 2, '
INGREDIENTES
- 200 Gramos de Ventresca de At�n
- 2 Cucharadas soperas de Mostaza antigua
- 1 Pizca de Sal y Pimienta
- 3 Cucharadas soperas de Salsa de soja
- Salsa tabasco

UTENSILIOS
- Bol

PASOS
1. Para hacer nuestro tartar de at�n lo primero que debes es cortar finamente el at�n. Como vez yo tengo dos filetes que he cortado con ayuda de un cuchillo con bastante filo.
El corte en este tipo de recetas es un tema controvertido ya que hay quienes prefieren trozos ligeramente grandes y otras personas que creen que lo mejor son cortes muy finos. Yo prefiero cortar la carne muy peque�a, casi como si fuese carne molida pero no triturada sino cortada a punta de cuchillo.
Puedes usar at�n fresco o at�n congelado, lo importante es que sea de calidad y que no tenga piel ni grasa.
2. Ahora tenemos que marinar el at�n, coloca entonces el pescado en un bol y agrega todos los ingredientes, de salsa picante solo a�ade unas 4 gotas. Revuelve todo hasta que se integren ambas salsas, comprueba la saz�n y corrige si es necesario agregando un poco m�s de cualquier ingrediente.
Reserva esto en la nevera una media hora para que el at�n absorba el sabor todo el sabor del marinado.
En esta recetas las medidas no tienen por que ser exactas, as� que modif�calas al gusto si prefieres que predomine alg�n sabor m�s que otro.
3. Pasado este tiempo, sirve el tartar de at�n picante en un bol o directamente sobre galletas saladas. Cuando coloques cada raci�n, aseg�rate de a�adir un gota de salsa sobre cada tostada o galleta, el �sabor ser� extraordinario!
Y si quieres otra versi�n de este aperitivo f�cil y r�pido, aqu� tienes otra receta de tartar de at�n. No olvides dejar tus comentarios.', 0)


insert into Recetas (Nombre,Tiempo,Tipo_id,Elaboracion,Favorito)
values ('Receta de pur� de Salm�n', 45, 2, '

INGREDIENTES
- 2 Filetes de Salm�n fresco
- 2 Cucharadas soperas de Pure de Patatas
- 1 Cucharada sopera de Queso crema
- 1 Taza de Pur� de espinacas
- 1 Cucharadita de Salsa negra
- 1 Unidad de Lim�n
- 1 Pizca de Or�gano
- 1 Pizca de Pasta de ajo
- 1 Pizca de Sal
- 1 Pizca de Pimienta negra

UTENSILIOS
- Sart�n
- Fog�n
- Bol

PASOS
1. Nuestro primer paso para realizar la receta de pur� de salm�n es prealistar los ingredientes. Aqu� puedes encontrar las recetas paso a paso de la pasta de ajo, el pur� de patatas y el pur� de espinacas.
2. Lleva una sart�n a fuego medio con un poquito de aceite de oliva, adiciona los dos filetes de salm�n con sal, pimienta negra y pasta de ajo al gusto. Dora el salm�n por ambos lados.
Una vez est� dorado el filete a�ade el zumo de lim�n a la sart�n junto con la salsa negra y el or�gano. Deja cocinar durante 2 minutos m�s y apaga el fuego.
En un procesador de alimentos adiciona el salm�n sin la piel. Procesa hasta obtener un pur�.A�ade ahora el pur� de espinacas y vuelve a procesar.
3. Por �ltimo, para hacer un poco m�s cremosa nuestra preparaci�n, adiciona el pur� de patatas y el queso crema. Estos ingredientes le dar�n un toque adicional a nuestra receta. Procesa nuevamente hasta obtenr una textura cremosa.
Con la ayuda de un molde con alguna forma divertida para ni�os adiciona el pur� de salm�n para darle forma. Si deseas puedes fre�r la piel del salm�n y ponerla de decoraci�n con un poco de queso parmesano encima esto dar� un toque crocante a la presentaci�n del pur� de salm�n.', 0) 

-- Recetas Query------

--select * from Recetas;

--Selecciona todos los ingredientes que tiene una receta--------

--select r.Nombre, Ingredientes  = STUFF((
--	select ', ' + i.Nombre from RecetasIngredientes ri
--	left join Ingredientes i 
--	on (i.Ingredientes_id = ri.Ingredientes_id)
--	where ri.Receta_id	= r.Receta_id FOR XML PATH('')),1, 1, '')
--from recetas r

----- Selecciona todos los utensilios que usa una receta------
--select r.Nombre, Utensilios = STUFF((
--	select ', ' + u.Nombre from RecetasUtensilios ru
--	left join Utensilios u 
--	on (u.Utensilios_id = ru.Utensilios_id)
--	where ru.Receta_id	= r.Receta_id FOR XML PATH('')),1, 1, '')
--from recetas r 

------ Selecciona las recetas que contengas los ingredientes----
--select rec.Receta_id, nombre from Recetas rec
--left join RecetasIngredientes rin on (rec.Receta_id = rin.Receta_id)
--where Ingredientes_id
--in(select Ingredientes_id from Ingredientes where nombre in ('arroz','ajo'))
--group by rec.Receta_id,Nombre having count(Ingredientes_id)=2

--select r.Nombre,concat(r.Tiempo, ' mins') as Tiempo, 
--	Utensilios = STUFF((
--	select ', ' + u.Nombre from RecetasUtensilios ru
--	left join Utensilios u 
--	on (u.Utensilios_id = ru.Utensilios_id)
--	where ru.Receta_id	= r.Receta_id FOR XML PATH('')),1, 1, ''),
--	Ingredientes = STUFF((
--	select ', ' + i.Nombre from RecetasIngredientes ri
--	left join Ingredientes i 
--	on (i.Ingredientes_id = ri.Ingredientes_id)
--	where ri.Receta_id	= r.Receta_id FOR XML PATH('')),1, 1, '')
--from recetas r

--select * from recetas;
--RECETAS VICTOR

insert into RecetasIngredientes
values(1,68),(1,29),(1,6),(1,27),(1,28),(1,15);

insert into RecetasIngredientes
values(2,1),(2,8),(2,14),(2,6),(2,4),(2,23),(2,26);

insert into RecetasIngredientes
values(3,1),(3,2),(3,21),(3,22),(3,66),(3,34),(3,9),(3,4),(3,23);

insert into RecetasIngredientes
values(4,1),(4,69),(4,20),(4,10),(4,19),(4,15),(4,7),(4,8);

insert into RecetasIngredientes
values(5,1),(5,4),(5,5),(5,6),(5,7),(5,8),(5,9),(5,10),(5,11),(5,15),(5,14),(5,12);

insert into RecetasUtensilios
values(1,1),(1,3), (2,2), (3,1),(3,2), (4,1), (5,2),(5,3);

--RECETAS de DAUD

insert into RecetasIngredientes
values(6,13),(6,61),(6,4),(6,120),(6,5),(6,59),(6,89);

insert into RecetasUtensilios
values(6,2),(6,1),(6,7);

insert into RecetasIngredientes
values(7,89),(7,10),(7,4),(7,76),(7,59),(7,12);

insert into RecetasUtensilios
values(7,2),(7,1),(7,7);

insert into RecetasIngredientes
values(8,118),(8,56),(8,55),(8,19),(8,57),(8,106),(8,87),(8,26);

insert into RecetasUtensilios
values(8,2),(8,1),(8,7);

insert into RecetasIngredientes
values(9,89),(9,58),(9,108),(9,22),(9,26),(9,87),(9,106);

insert into RecetasUtensilios
values(9,2),(9,1),(9,7);

insert into RecetasIngredientes
values(10,124),(10,60),(10,62),(10,64),(10,13),(10,63),(10,106),(10,65);

insert into RecetasUtensilios
values(10,3),(10,1),(10,7);

insert into RecetasIngredientes
values(11,95),(11,22),(11,70),(11,83);

insert into RecetasUtensilios
values(11,7),(11,2),(11,8),(11,6);

insert into RecetasIngredientes
values(12,22),(12,70),(12,97),(12,42),(12,81),(12,121);

insert into RecetasUtensilios
values(12,8),(12,6),(12,3);

insert into RecetasIngredientes
values(13,122),(13,22),(13,109),(13,70),(13,97);

insert into RecetasUtensilios
values(13,6),(13,8),(13,3);

--RECETAS MONICA

insert into RecetasIngredientes
values(14,26),(14,33),(14,117),(14,87),(14,42),(14,84),(14,125),(14,49);

insert into RecetasUtensilios
values(14,6);

insert into RecetasIngredientes 
values(15,45),(15,126),(15,10),(15,4),(15,26),(15,125),(15,127);

insert into RecetasUtensilios
values(15,5);

insert into RecetasIngredientes
values (16,46),(16,33),(16,47),(16,125);

insert into RecetasUtensilios
values (16,6);

insert into RecetasIngredientes
values (17,33),(17,4),(17,6),(17,26),(17,103),(17,47),(17,87);

insert into RecetasUtensilios
values (17,6);

insert into RecetasIngredientes
values (18,4),(18,17),(18,87),(18,10),(18,125),(18,30);

insert into RecetasUtensilios
values (18,1),(18,7);

insert into RecetasIngredientes 
values(19,33),(19,21),(19,26),(19,87),(19,125);

insert into RecetasUtensilios
values (19,1),(19,7);

insert into RecetasIngredientes
values (20,110),(20,10),(20,87),(20,106),(20,125),(20,4);

insert into RecetasUtensilios
values (20,3),(20,1);

insert into RecetasIngredientes
values (21,4),(21,6),(21,14),(21,26),(21,125),(21,106);

insert into RecetasUtensilios
values (21,2),(21,7);

insert into RecetasIngredientes
values (22,14),(22,91),(22,125);

insert into RecetasUtensilios
values (22,2),(22,3);

--RECETAS CRISTIAN

insert into RecetasIngredientes
values (29,90),(29,12),(29,123),(29,4),(29,101),(29,78),(29,97),(29,23),(29,125),(29,106);

insert into RecetasUtensilios
values (29,1),(29,2),(29,7);

insert into RecetasIngredientes
values (30,71),(30,12),(30,82),(30,22),(30,4),(30,91);

insert into RecetasUtensilios
values (30,2),(30,9),(30,7);

insert into RecetasIngredientes
values (31,71),(31,26),(31,104),(31,87),(31,91),(31,23),(31,103);

insert into RecetasUtensilios
values (31,2),(31,1),(31,7),(31,3);

insert into RecetasIngredientes
values (32,71), (32,26), (32,104),(32,87),(32,91),(32,23),(32,103),(32,125);

insert into RecetasUtensilios
values (32,7),(32,1);

insert into RecetasIngredientes
values (33,73),(33,13),(33,125),(33,42),(33,112);

insert into RecetasUtensilios
values (33,9);

insert into RecetasIngredientes
values (34,111),(34,34),(34,72),(34,22),(34,91),(34,125),(34,106),(34,26),(34,103);

insert into RecetasUtensilios
values (34,8),(34,1),(34,7);

insert into RecetasIngredientes
values (35,54),(35,4),(35,10),(35,77),(35,14),(35,26),(35,105),(35,106),(35,85),(35,74),(35,91),(35,125);

insert into RecetasUtensilios
values (35,1),(35,7),(35,8);

insert into RecetasIngredientes
values (36,54),(36,125),(36,13),(36,91),(36,22);

insert into RecetasUtensilios
values (36,8),(36,1),(36,7);

insert into RecetasIngredientes
values (37,53),(37,52),(37,125),(37,106),(37,113),(37,115);

insert into RecetasUtensilios
values (37,8);

insert into RecetasIngredientes
values (38,50),(38,107),(38,14),(38,21),(38,114),(38,42),(38,26),(38,125),(38,106);

insert into RecetasUtensilios
values (38,8),(38,7),(38,1);
    
-- FIN RECETAS CRISTIAN

-- AGREGANDO LAS IMAGENES
declare @receta_id int set @receta_id=1;

while (@receta_id <= (select count(*) from Recetas)) 
BEGIN
insert into Imagenes(Imagen_id,Nombre) values (@receta_id,
	(select REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
		LOWER(Nombre), ' ', '_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n'),',','')
		from Recetas where Receta_id=@receta_id)
	)
	set @receta_id=@receta_id+1;
END
-- FIN DE AGREGAR IMAGENES

PRINT 'La base de datos se ha creado correctamente';